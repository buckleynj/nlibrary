package nLibrary;

import javax.swing.SwingWorker;

/**
 * The BackgroundProcess SwingWorker handles periodically writing the log buffer to the disk based on the write frequency (defaults to every 60 seconds) and when the process is terminated.
 * @author Nathan Buckley
 *
 */
public class LoggingProcess extends SwingWorker<Void, Void> {
	
	/** <b>writeFrequency</b> (in milliseconds) is the delay between each processing of the background thread. In other words, while messages can be written to the log buffer at any time, they will be written to this disk only periodically, and that rate is specified by this value. */
	static int writeFrequency = 10000;  // 10 seconds

	
	public LoggingProcess() {
		nCore.print("loggingProcess initiated ("+String.valueOf(writeFrequency/1000)+"-second write frequency).","LOGGING");
	}
	
	/**
	 * Set the process rate (in seconds).
	 * @param rate
	 */
	public void setProcessRate(int rate) {
		writeFrequency = rate*1000;
		nCore.print("Write frequency set to once every "+String.valueOf(writeFrequency/1000)+"s.","LOGGING");
	}
	
	/**
	 * Gets the current process rate (in seconds).
	 * @return
	 */
	public int getProcessRate() {
		return writeFrequency/1000;
	}
	
	/**
	 * Runs the background process, writing the write buffer to the disk periodically.
	 * 
	 * @param pause - (optional) Determines if the process pauses for a duration equal to the writeFrequency. If omitted, defaults to yes.
	 */
	public void runProcess() {
		runProcess(true);
	}
	
	/**
	 * Runs the background process, writing the write buffer to the disk periodically.
	 * 
	 * @param pause - (optional) Determines if the process pauses for a duration equal to the writeFrequency. If omitted, defaults to yes.
	 */
	public void runProcess(boolean pause) {
		nFile.flushLogBuffer();
		
		if( pause )
			try { Thread.sleep(writeFrequency); } catch (InterruptedException e) { nCore.handleStackTrace(e); }
	}

	@Override
	protected Void doInBackground() {
		try {
			// set up a process loop to periodically write to the disk
			while (!isCancelled()) {
				runProcess();
			}
		}
		catch(Exception e) {
			nCore.handleStackTrace(e);
		}
		nCore.print("loggingProcess wrapping up.","LOGGING");
		return null;
	}

	@Override
	public void done() {
		nFile.backgroundLoggingProcessInitiated = false;
		nCore.print("loggingProcess finished.","LOGGING");
		nFile.flushLogBuffer();  // flush anything remaining in the buffer
	}
}
