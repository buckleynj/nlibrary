package nLibrary;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * A class for handling file system operations.
 * @author Nathan Buckley
 *
 */
public class nFile {
	
	/**
	 * The LoggingProcess global.
	 */
	static LoggingProcess lp;
	
	/**
	 * Determines whether we are logging by default or not.
	 */
	static boolean areLogging = false;
	
	/**
	 * The background process which handles logging.
	 */
	public static boolean backgroundLoggingProcessInitiated = false;
	/**
	 * We use this to store a buffer of log messages that are written periodically to the log file.
	 */
	static StringBuilder logbuffer = new StringBuilder();
	
	/**
	 * We set this to true if for some reason we want to prevent the log buffer from being flushed or written out to the log file.
	 * The log buffer itself will still be written to, but nothing will be written to the disk until this is false.<br>
	 * We do this if for example we want to change the log file. We'd want to pause the logbuffer from being written to the disk until
	 * we create the new log file and then we can resume it once again.
	 */
	public static boolean holdLogBuffer = false;
	
	/**
	 * Location of the log file.
	 */
	static Path logPath;
	
	/**
	 * Established charset for read operations.<br>
	 * Use setReadCharset() to set.
	 */
	static Charset readCharset = Charset.forName("UTF-8");
	
	/**
	 * Established charset for write operations.<br>
	 * use setWriteCharset() to set.
	 */
	static Charset writeCharset = Charset.forName("UTF-8");
	
	/** The system default file separator.<br>System.getProperty("File.separator") */
	public final static String fs = System.getProperty("file.separator");
	
	/** The system default line separator.<br>System.getProperty("line.separator") */
	public final static String nl = System.getProperty("line.separator");
	
	/**
	 * Sets the charset to use for read operations.
	 * @param charset - String charset name or charset object itself
	 */
	public static void setReadCharset(String charset) {
		if( Charset.isSupported(charset) )
			readCharset = Charset.forName(charset);
		else
			nCore.print("setReadCharset(): Charset supplied not available on system. Maintaining previous charset setting.","WARNING");
	}
	
	/**
	 * Sets the charset to use for read operations.
	 * @param charset - String charset name or charset object itself
	 */
	public static void setReadCharset(Charset charset) {
		readCharset = charset;
	}
	
	/**
	 * Sets the charset to use for write operations.
	 * @param charset - String charset name or charset object itself
	 */
	public static void setWriteCharset(String charset) {
		if( Charset.isSupported(charset) )
			writeCharset = Charset.forName(charset);
		else
			nCore.print("setWriteCharset(): Charset supplied not available on system. Maintaining previous charset setting.","WARNING");
	}
	
	/**
	 * Sets the charset to use for write operations.
	 * @param charset - String charset name or charset object itself
	 */
	public static void setWriteCharset(Charset charset) {
		writeCharset = charset;
	}
	
	
	/**
	 * Enables or disables logging globally. 
	 * @param logging
	 */
	public static boolean setLogging(boolean logging) {
		// only set logging if we have a log path
		if( logging && nFile.logPath != null ) {
			areLogging = true;
			
			// now initiate a background process to periodically flush the buffer
			// but first cancel any existing one if it exists
			
			lp = new LoggingProcess();
			if( lp != null && !lp.isDone() )
				nFile.backgroundLoggingProcessInitiated = true;
			else
				nCore.print("nFile.setLogging(): logging process is null or already done.","WARNING");
			
			/*
			lp.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					String name = evt.getPropertyName();
					if (name.equals("progress")) {
						// do nothing for now
					} else if (name.equals("state")) {
						// do nothing for now
					}
				}
			});
			*/
			lp.execute();
		}
		else {
			areLogging = false;
			
			// we can stop the background log process if it is going
			if( lp != null && !lp.isDone() )
				lp.cancel(true);
		}
		return areLogging;
	}
	
	/**
	 * Setup the log file. If no parameters are entered, the log file is saved in the current folder with the name "log" appended with a datetime string.<br>
	 * Accepts a String path or Path path.
	 * @param file - (optional) Path variable or String variable associated with the log file location. If omitted, will save to the run directly in the format: log_date-time.txt
	 * @return the file variable assocated with the log file
	 */
	public static File setLogFile() {
		return setLogFile("log_"+nTime.getStringFromDate("dd-MM-yyyy_HH-mm-ss")+".txt");
	}
	
	/**
	 * Setup the log file. If no parameters are entered, the log file is saved in the current folder with the name "log" appended with a datetime string.<br>
	 * Accepts a String path or Path path.
	 * @param file - (optional) Path variable or String variable associated with the log file location. If omitted, will save to the run directly in the format: log_date-time.txt
	 * @return the file variable assocated with the log file
	 */
	public static File setLogFile(String path) {
		Path p = Paths.get(path);
		if( p.toFile().isDirectory() ) {
			nCore.print("setLogFile() given a String location that appears to be a directory, and not a file location. Using default.", "WARNING");
			return setLogFile();
		}
		return setLogFile(p);
	}
	
	/**
	 * Setup the log file. If no parameters are entered, the log file is saved in the current folder with the name "log" appended with a datetime string.<br>
	 * Accepts a String path or Path path.
	 * @param file - (optional) Path variable or String variable associated with the log file location. If omitted, will save to the run directly in the format: log_date-time.txt
	 * @return the file variable associated with the log file
	 */
	public static File setLogFile(Path file) {
		// create the file, see if it's writable
		if( !writeFile(file,"LOG FILE CREATED "+nLibrary.nTime.getStringFromDate("MM/dd/yyyy HH:mm:ss")+" (month/day/year hour:minute:second)"+nl) ) {
			nCore.print("nCore.setLogFile(): Unable to create log file.","WARNING");
			return null;
		}
		if( !file.toFile().canWrite() ) {
			nCore.print("nCore.setLogFile(): The log file either doesn't exist, or is unwriteable.","WARNING");
			return null;
		}
		
		// if we made it this far, we're good. See the logPath and return the File
		logPath = file;
		return file.toFile();
	}

  /**
   * Adds a message to the logbuffer.<br>
   * Will only actually write to the log if logging is enabled (nCore.areLogging == true), the log path is valid, and we are not currently hodling the log buffer.<br>
   * @param msg - (String) the message to write
   * @param source - (String) the source of the message, i.e. the function or part of the code where the log() message is coming from
   * @param category - (String) the category of the message
   * @param uniqueid - (String) the unique identifer of the message
   */
	public static void log(String msg) {
		log(msg,"-","-","-");
	}
	
  /**
   * Adds a message to the logbuffer.<br>
   * Will only actually write to the log if logging is enabled (nCore.areLogging == true), the log path is valid, and we are not currently hodling the log buffer.<br>
   * @param msg - (String) the message to write
   * @param source - (String) the source of the message, i.e. the function or part of the code where the log() message is coming from
   * @param category - (String) the category of the message
   * @param uniqueid - (String) the unique identifer of the message
   */
  public static void log(String msg, String source) {
    log(msg,source,"UNCATEGORIZED","-");
  }
  
  /**
   * Adds a message to the logbuffer.<br>
   * Will only actually write to the log if logging is enabled (nCore.areLogging == true), the log path is valid, and we are not currently hodling the log buffer.<br>
   * @param msg - (String) the message to write
   * @param source - (String) the source of the message, i.e. the function or part of the code where the log() message is coming from
   * @param category - (String) the category of the message
   * @param uniqueid - (String) the unique identifer of the message
   */
  public static void log(String msg, String source, String category) {
    log(msg,source,category,"-");
  }
	
  /**
   * Adds a message to the logbuffer.<br>
   * Will only actually write to the log if logging is enabled (nCore.areLogging == true), the log path is valid, and we are not currently hodling the log buffer.<br>
   * @param msg - (String) the message to write
   * @param source - (String) the source of the message, i.e. the function or part of the code where the log() message is coming from
   * @param category - (String) the category of the message
   * @param uniqueid - (String) the unique identifer of the message
   */
	public static void log(String msg, String source, String category, String uniqueid) {
		if( areLogging ) {
			// if there's no message, return, otherwise trim it down
			if( msg == null || msg.trim().equals("") )
				return;
			else
				msg = msg.trim();
			
			Date d = new Date();
			SimpleDateFormat ft_LOCAL = new SimpleDateFormat ("hh:mm:ss a");
			SimpleDateFormat ft_GMT = new SimpleDateFormat ("HH:mm:ss");
			ft_GMT.setTimeZone(TimeZone.getTimeZone("UTC"));
			String tLine = String.valueOf(d.getTime()) + "\t"
			    + ft_GMT.format(d) + "\t"
			    + ft_LOCAL.format(d) + "\t"
			    + (nCore.startTime==-1?"-":nTime.getElapsedTime(d.getTime())) + "\t"
			    + category + "\t"
			    + uniqueid + "\t"
			    + source + "\t"
			    + msg;
	
			logline(tLine);
		}
		else {
			System.out.println("Cannot log message because logging is not enabled."+msg);
		}
	}
	
	/**
	 * Adds a message to the logbuffer.<br>
	 * Use log(String msg, String source, String category, String uniqueid) if you want to write an entry to the logs without printing it to the console.<br>
	 * @param msg
	 */
	public static void logline(String msg) {
		logbuffer.append(msg).append(nl);
	}
	
	/** Clears the existing log buffer. */
	public static void clearLogBuffer() {
		//nCore.print("nFile.clearLogBuffer(): The log buffer is cleared.", "WARNING","-",false);
		logbuffer.setLength(0);
	}
	
	/** 
	 * Writes the existing log buffer to the log file and clears the buffer and <b>return 0</b>.<br>
	 * If it cannot write to the log for some reason, it will return another number:
	 * <ol>
	 * <li>Logging isn't enabled</li>
	 * <li>The log path is null</li>
	 * <li>The log buffer is empty</li>
	 * <li>The log buffer is being held</li>
	 * <li>Unknown logging exception</li>
	 * </ol>
	 */
	final public static int flushLogBuffer() {
		try {
			// first make sure the buffer isn't currently empty
			if( areLogging ) {
				if( logPath != null ) {
					if( logbuffer.length() > 0 ) {
						if( !holdLogBuffer ) {
							byte[] bytes = logbuffer.toString().getBytes(writeCharset);
							Files.write(logPath, bytes, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
							//nCore.print("The log buffer was written to the log file: "+logPath.toString(),"nFile.flushLogBuffer()","WARNING","-",false);
							clearLogBuffer();
							return 0;
						}
						else {
							//nCore.print("The log buffer is being held so nothing will be written to the logs.","nFile.flushLogBuffer()","WARNING","-",false);
							return 4;
						}
					}
					else {
						//nCore.print("The log buffer is empty so there's nothing to flush.","nFile.flushLogBuffer()","WARNING","-",false);
						return 3;
					}
				}
				else {
					nCore.print("The log path is null so we can't write to the log.","nFile.flushLogBuffer()","WARNING","-",false);
					return 2;
				}
			}
			else {
				//nCore.print("We aren't logging so there's no need to flush the log buffer.","nFile.FlushLogBuffer()","WARNING","-",false);
				return 1;
			}
		}
		catch(Exception e) {
			nCore.handleStackTrace(e);
		}
		return 5;
	}
	
	/** 
	 * Manually add a line to the log file via a popup prompt.
	 */
	public void note() {
		note(null);
	}
	
	/** 
	 * Manually add a line to the log file via a popup prompt.
	 */
	public void note(Component parentComponent) {
		String s = (String) JOptionPane.showInputDialog(parentComponent, "Enter a note to be logged:", "Note Entry", JOptionPane.PLAIN_MESSAGE, null, null, "");
		if ((s != null) && (s.length() > 0)) {
			nCore.print(s,"NOTE");
		}
	}

	/**
	 * Writes data to a file.
	 * @param path - (Path) location of the file (use Paths.get(".") for the current directory, append with desired file name)
	 * @param data - (String) data to be written. This does not automatically add a newline so be sure to add it if you want one.
	 * @param charset - (Charset) chartset to use (optional, defaults to global writeCharset setting)
	 * @param oo - (OpenOption) the open option (optional, e.g. StandardOpenOption.APPEND). If no options are present then this method works as if the CREATE, TRUNCATE_EXISTING, and WRITE options are present. Sample code:<br><br>
	 * <code>OpenOption[] options = new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.APPEND };</code><br>
	 * @return
	 */
	public static boolean writeFile(Path path, String data) {
		return writeFile(path, data, writeCharset, null);
	}

	/**
	 * Writes data to a file.
	 * @param path - (Path) location of the file (use Paths.get(".") for the current directory, append with desired file name)
	 * @param data - (String) data to be written. This does not automatically add a newline so be sure to add it if you want one.
	 * @param charset - (Charset) chartset to use (optional, defaults to global writeCharset setting)
	 * @param oo - (OpenOption) the open option (optional, e.g. StandardOpenOption.APPEND). If no options are present then this method works as if the CREATE, TRUNCATE_EXISTING, and WRITE options are present. Sample code:<br><br>
	 * <code>OpenOption[] options = new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.APPEND };</code><br>
	 * @return
	 */
	public static boolean writeFile(Path path, String data, Charset charset) {
		return writeFile(path, data, charset, null);
	}

	/**
	 * Writes data to a file.
	 * @param path - (Path) location of the file (use Paths.get(".") for the current directory, append with desired file name)
	 * @param data - (String) data to be written. This does not automatically add a newline so be sure to add it if you want one.
	 * @param charset - (Charset) chartset to use (optional, defaults to global writeCharset setting)
	 * @param oo - (OpenOption) the open option (optional, e.g. StandardOpenOption.APPEND). If no options are present then this method works as if the CREATE, TRUNCATE_EXISTING, and WRITE options are present. Sample code:<br><br>
	 * <code>OpenOption[] options = new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.APPEND };</code><br>
	 * @return
	 */
	public static boolean writeFile(Path path, String data, Charset charset, OpenOption[] oo) {
		byte[] bytes = data.getBytes(charset);
    	try {
    		if( oo==null )
    			Files.write(path, bytes);
    		else
    			Files.write(path, bytes, oo);
		} catch (IOException e) {
			nCore.handleStackTrace(e);
			return false;
		}
		return true;
	}
	
	
	/**
	 * Reads a file on the file system and returns a String with that data.
	 * @param path - (Path|File) location of the file to be read
	 * @param charset - (Charset) Charset of the file (optional)
	 * @return A String containing the file data.
	 */
	public static String readFile(File file) {
		return readFile(file.toPath(),readCharset);
	}
	
	/**
	 * Reads a file on the file system and returns a String with that data.
	 * @param path - (Path|File) location of the file to be read
	 * @param charset - (Charset) Charset of the file (optional)
	 * @return A String containing the file data.
	 */
	public static String readFile(File file,Charset charset) {
		return readFile(file.toPath(), charset);
	}
	
	/**
	 * Reads a file on the file system and returns a String with that data.
	 * @param path - (Path|File) location of the file to be read
	 * @param charset - (Charset) Charset of the file (optional)
	 * @return A String containing the file data.
	 */
	public static String readFile(Path path) {
		return readFile(path,readCharset);
	}
	
	/**
	 * Reads a file on the file system and returns a String with that data.
	 * @param path - (Path) location of the file to be read
	 * @param charset - (Charset) Charset of the file (optional)
	 * @return A String containing the file data.
	 */
	public static String readFile(Path path, Charset charset) {
		if( Files.notExists(path) ) {
			nCore.print("readFile(): File not found. Aborting read...","WARNING");
			return null;
		}
		
		// make sure it's readable
    	if( !Files.isReadable(path) ) {
    		nCore.print("readFile(): File is not readable. Aborting read...","WARNING");
    		return null;
    	}
    	
    	// load contents
    	byte[] fileArray = null;
    	try {
			fileArray = Files.readAllBytes(path);
		} catch (IOException e) {
			nCore.handleStackTrace(e);
			return null;
		}
    	if( fileArray == null ) {
    		nCore.print("readFile(): Could not read from the specified file. Aborting read...","WARNING");
    		return null;
    	}

    	return new String(fileArray,charset);
	}
	
	/** Delete a file on the filesystem.
	 * 
	 * @param path
	 * @return boolean true or false if the file was deleted or not.
	 */
	public static boolean deleteFile(Path path) {
		return deleteFile(path.toFile());
	}
	
	/** Delete a file on the filesystem.
	 * 
	 * @param path
	 * @return boolean true or false if the file was deleted or not.
	 */
	public static boolean deleteFile(File file) {
		return file.delete();
	}
	
	/**
	 * Opens a file browser and returns an array of the chosen files. Returns null if operation was cancelled.
	 * @param title - (String) Title of the dialog
	 * @param approveBtnText - (String) The text of the Approve button (i.e. "Select File" or "Select Folder", etc.)
	 * @param openingLocation - (String | Path | File) The initial location of the dialog
	 * @param defaultFileName - (String) The default name of the file in the file name text field (leave null if you want none).
	 * @param fileSelectionMode - (int) Sets the file selection mode (e.g. JFileChooser.DIRECTORIES_ONLY)
	 * @param fileNameExtensionFilter - (String[]) Each entry should be in the format "nCTOP files (.ctop);ctop" (no quotes), with the semi-colon separating the name and the extension..
	 * @param allowMultiSelection - (boolean) Set to true to allow the selection of more than 1 file
	 */
	public static File[] Browse(String title, String approveBtnText, String openingLocation, String defaultFileName, int fileSelectionMode, String[] fileNameExtensionFilter, boolean allowMultiSelection) {
		return Browse(title,approveBtnText,Paths.get(openingLocation).toFile(),defaultFileName,fileSelectionMode,fileNameExtensionFilter,allowMultiSelection);
	}
	
	/**
	 * Opens a file browser and returns an array of the chosen files. Returns null if operation was cancelled.
	 * @param title - (String) Title of the dialog
	 * @param approveBtnText - (String) The text of the Approve button (i.e. "Select File" or "Select Folder", etc.)
	 * @param openingLocation - (String | Path | File) The initial location of the dialog
	 * @param defaultFileName - (String) The default name of the file in the file name text field (leave null if you want none).
	 * @param fileSelectionMode - (int) Sets the file selection mode (e.g. JFileChooser.DIRECTORIES_ONLY)
	 * @param fileNameExtensionFilter - (String[]) Each entry should be in the format "nCTOP files (.ctop);ctop" (no quotes), with the semi-colon separating the name and the extension..
	 * @param allowMultiSelection - (boolean) Set to true to allow the selection of more than 1 file
	 */
	public static File[] Browse(String title, String approveBtnText, Path openingLocation, String defaultFileName, int fileSelectionMode, String[] fileNameExtensionFilter, boolean allowMultiSelection) {
		return Browse(title,approveBtnText,openingLocation.toFile(),defaultFileName, fileSelectionMode,fileNameExtensionFilter,allowMultiSelection);
	}
	
	/**
	 * Opens a file browser and returns an array of the chosen files. Returns null if operation was cancelled.
	 * @param title - (String) Title of the dialog
	 * @param approveBtnText - (String) The text of the Approve button (i.e. "Select File" or "Select Folder", etc.)
	 * @param openingLocation - (String | Path | File) The initial location of the dialog
	 * @param defaultFileName - (String) The default name of the file in the file name text field (leave null if you want none).
	 * @param fileSelectionMode - (int) Sets the file selection mode (e.g. JFileChooser.DIRECTORIES_ONLY)
	 * @param fileNameExtensionFilter - (String[]) Each entry should be in the format "nCTOP files (.ctop);ctop" (no quotes), with the semi-colon separating the name and the extension..
	 * @param allowMultiSelection - (boolean) Set to true to allow the selection of more than 1 file
	 */
	public static File[] Browse(String title, String approveBtnText, File openingLocation, String defaultFileName, int fileSelectionMode, String[] fileNameExtensionFilter, boolean allowMultiSelection) {
		try {
			JFileChooser fc = new JFileChooser();
	    	fc.setDialogTitle(title);
	    	if( fileNameExtensionFilter!=null ) {
	    		if( fileNameExtensionFilter.length>0)
	    			fc.setFileFilter(new FileNameExtensionFilter(fileNameExtensionFilter[0].split(";")[0],fileNameExtensionFilter[0].split(";")[1]));
	    		if( fileNameExtensionFilter.length>1) {
		    		for(int i=1;i<fileNameExtensionFilter.length;i++) {
		    			fc.addChoosableFileFilter(new FileNameExtensionFilter(fileNameExtensionFilter[i].split(";")[0],fileNameExtensionFilter[i].split(";")[1]));
		    		}
	    		}
	    	}
	    	
    		fc.setApproveButtonText(approveBtnText);
			fc.setFileSelectionMode(fileSelectionMode);
	    	fc.setMultiSelectionEnabled(allowMultiSelection);
	    	
	    	// set open location
	    	if( openingLocation==null ) {
	    		// use the current directory (where the application is being run)
	    		fc.setCurrentDirectory(Paths.get(".").toFile());
			}
			else {
				//if the supplied location was a file, use that files directory instead
				if (!openingLocation.isDirectory()) {
					openingLocation = Paths.get(openingLocation.getParent()).toAbsolutePath().toFile();
				}
				fc.setCurrentDirectory(openingLocation);
			}
	    	
	    	if( defaultFileName!=null && !defaultFileName.trim().equals("") )
	    		fc.setSelectedFile(new File(defaultFileName));
	    	
	    	// show the dialog
			int returnVal = fc.showOpenDialog(fc);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				if( allowMultiSelection )
					return fc.getSelectedFiles();
				else {
					File[] f = new File[1];
					f[0] = fc.getSelectedFile();
					return f;
				}
			}
			else {
				// cancelled by user
				return null;
			}
		}
		catch(Exception e) {
			nCore.handleStackTrace(e);
			return null;
		}
	}
}
