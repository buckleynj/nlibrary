package nLibrary;
import java.awt.AlphaComposite;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.text.Position;


/**
 * Class for doing general GUI manipulations.
 * @author Nathan Buckley
 *
 */
public class nInterface {
	
	public static String systemDefaultTerm = "<System default>";

	// global values for FadeLabel icon images
	public static BufferedImage success_icon;
	public static BufferedImage failure_icon;
	public static BufferedImage warning_icon;
	
	
	// https://stackoverflow.com/questions/13203415/how-to-add-fade-fade-out-effects-to-a-jlabel
    public static class FadeLabel extends JLabel {

		private static final long serialVersionUID = -2741269311741387811L;
		private BufferedImage icon;  // the current icon in use
		private float alpha;
        
        private boolean isFadingNow = false;
        public FadeLabel thisObject;
        private Timer timer;
        private ActionListener al = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float alpha = thisObject.getAlpha();
                alpha -= 0.05;
                if(alpha<=0) {
                	thisObject.setAlpha(0);
                	setVisible(false);
                	isFadingNow = false;
                	timer.stop();
                }
                else {
                	thisObject.setAlpha(alpha);
                }
            }
        };

        public FadeLabel() {
        	setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);
            setAlpha(1f);
            thisObject=this;
        }
        
        public FadeLabel(URL success_icon_url) {
        	try { success_icon = ImageIO.read(success_icon_url); } catch (Exception e) {}
            setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);
            setAlpha(1f);
            thisObject=this;
        }
        
        public FadeLabel(URL success_icon_url, URL failure_icon_url) {
        	try {
        		success_icon = ImageIO.read(success_icon_url);
        		failure_icon = ImageIO.read(failure_icon_url);
        	} catch (Exception e) {
        	}
        	setHorizontalAlignment(CENTER);
        	setVerticalAlignment(CENTER);
        	setAlpha(1f);
        	thisObject=this;
        }
        
        public FadeLabel(URL success_icon_url, URL failure_icon_url, URL warning_icon_url) {
        	try {
        		success_icon = ImageIO.read(success_icon_url);
        		failure_icon = ImageIO.read(failure_icon_url);
        		warning_icon = ImageIO.read(warning_icon_url);
        	} catch (Exception e) {
        	}
        	setHorizontalAlignment(CENTER);
        	setVerticalAlignment(CENTER);
        	setAlpha(1f);
        	thisObject=this;
        }

        public void setAlpha(float value) {
            if (alpha != value) {
                float old = alpha;
                alpha = value;
                firePropertyChange("alpha", old, alpha);
                repaint();
            }
        }

        public float getAlpha() {
            return alpha;
        }
        
        /**
         * Positive feedback for successful events: A green 'check' icon is shown (for 5 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showConfirmation() {
        	show(5000, success_icon, "");
        }
        
        /**
         * Positive feedback for successful events: A green 'check' icon is shown (for 5 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showConfirmation(int wait) {
        	show(wait, success_icon, "");
        }
        
        /**
         * Positive feedback for successful events: A green 'check' icon is shown (for 5 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showConfirmation(String tooltipText) {
        	show(5000, success_icon, tooltipText);
        }
        
        /**
         * Positive feedback for successful events: A green 'check' icon is shown (for 5 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showConfirmation(int wait, String tooltipText) {
        	show(wait, success_icon, tooltipText);
        }
        
        /**
         * Positive feedback for failed events: A red 'x' icon is shown (for 30 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showFailure() {
        	show(30000, failure_icon, "");
        }
        
        /**
         * Positive feedback for failed events: A red 'x' icon is shown (for 30 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showFailure(int wait) {
        	show(wait, failure_icon, "");
        }
        
        /**
         * Positive feedback for failed events: A red 'x' icon is shown (for 30 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showFailure(int wait, String tooltipText) {
        	show(wait, failure_icon, tooltipText);
        }
        
        /**
         * Positive feedback for partially successful events: A yellow/orange '!' icon is shown (for 30 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showWarning() {
        	show(30000, warning_icon, "");
        }
        
        /**
         * Positive feedback for partially successful events: A yellow/orange '!' icon is shown (for 30 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showWarning(int wait) {
        	show(wait, warning_icon, "");
        }
        
        /**
         * Positive feedback for partially successful events: A yellow/orange '!' icon is shown (for 30 seconds by default) and fades away.
         * @param wait in milliseconds - (optional) the amount of time to show the icon before it fades away
         * @param tooltipText - (optional) the text to show when the user hovers over the icon
         */
        public void showWarning(int wait, String tooltipText) {
        	show(wait, warning_icon, tooltipText);
        }
        
        public void show(int wait, BufferedImage i, String tooltipText) {
        	reset();
        	icon = i;
        	thisObject.setToolTipText(tooltipText);
        	setVisible(true);
        	repaint();
        	isFadingNow = true;
        	Runnable task = () -> {
	        	fadeOut(wait);
        	};
        	Thread fadeOutThread = new Thread(task);
        	fadeOutThread.start();
        }
        
        /**
         * Stop the timer and cancel any fade in progress.
         */
        public void reset() {
        	if( isFadingNow ) {
        		isFadingNow = false;
        		if(timer!=null && timer.isRunning())
            		timer.stop();
        	}
        	setVisible(false);
    		setAlpha(1f);
        }
        
        private void fadeOut(int delay) {
        	if(timer!=null && timer.isRunning())
        		timer.stop();
        	timer = new Timer(100, al);
            //timer.setRepeats(true);
            timer.setCoalesce(true);
            timer.setInitialDelay(delay);
            timer.start();
        }

        @Override
        public Dimension getPreferredSize() {
            return icon == null ? super.getPreferredSize() : new Dimension(icon.getWidth(), icon.getHeight());
        }

        @Override
        public void paint(Graphics g) {
            // This is one of the few times I would directly override paint
            // This makes sure that the entire paint chain is now using
            // the alpha composite, including borders and child components
            Graphics2D g2d = (Graphics2D) g.create();
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, getAlpha()));
            super.paint(g2d);
            g2d.dispose();
        }

        @Override
        protected void paintComponent(Graphics g) {
            // This is one of the few times that doing this before the super call
            // will work...
            if (icon != null) {
                int x = (getWidth() - icon.getWidth()) / 2;
                int y = (getHeight() - icon.getHeight()) / 2;
                g.drawImage(icon, x, y, this);
            }
            super.paintComponent(g);
        }
    }
	
	/**
	 * Move a set of selected items from one JList (using DefaultListModel&lt;String&gt;) to another.<br>
	 * The selection remains after the move to the new list.<br>
	 * If the either JList is not using DefaultListModel format, it will attempt to convert them
	 * @param origin - the originating JList
	 * @param destination - the destination JList
	 * @param withSort - if true, the destination list will be sorted after the move
	 */
	public static void moveSelectedListItems(javax.swing.JList<String> origin, javax.swing.JList<String> destination, boolean withSort) {
		if( !origin.isSelectionEmpty() ) {
			int[] LoggableEventsSelectedIndices = origin.getSelectedIndices();  // get all the selected indices (int)
			
			// for some reason this doesn't always work
			DefaultListModel<String> modeldestination;
			DefaultListModel<String> modelorigin;
			try {
				modeldestination = (DefaultListModel<String>) destination.getModel();
				modelorigin = (DefaultListModel<String>) origin.getModel();
			} catch(ClassCastException cce) {
				// this happens if the list is not a DefaultListModel (probably a ListModel or AbstractListModel or something)
				// we can still manually convert and replace the model
				nLibrary.nCore.print("nLibrary.nCore.nInterface.moveSelectedListItems(): Cant cast DefaultListModel; manually creating.","WARNING");
				// so just get the list models then
				ListModel<String> lmodeldestination = destination.getModel();
				ListModel<String> lmodelorigin = origin.getModel();

				// create a new DefaultListModel and all all the contents of the destination list model to it
				modeldestination = new DefaultListModel<String>();
				for(int i=0;i<lmodeldestination.getSize();i++)
					modeldestination.addElement(lmodeldestination.getElementAt(i));
				
				// create a new DefaultListModel and all all the contents of the origin list model to it
				modelorigin = new DefaultListModel<String>();
				for(int i=0;i<lmodelorigin.getSize();i++)
					modelorigin.addElement(lmodelorigin.getElementAt(i));
				
				// now reapply them to the GUIs
				origin.setModel(modelorigin);
				destination.setModel(modeldestination);
			}
			
			ArrayList<String> selectedValues = null;  // create an arraylist to store the selected values
			if( LoggableEventsSelectedIndices != null && LoggableEventsSelectedIndices.length > 0 ) {  // if there is at least 1 index selected
				selectedValues = new ArrayList<String>();  // create  the arraylist to store the selected values (we need this to remember the values by name, because the indices will change when we remove the first one)
				// then add the selected values (Strings) to the other model, and selectedValues<>
				for( int i : LoggableEventsSelectedIndices ) {
					modeldestination.addElement(modelorigin.get(i));
					selectedValues.add(modelorigin.get(i));
				}
				// now remove them from the originating list
				for(int i=0;i<selectedValues.size();i++) {
					modelorigin.remove(origin.getNextMatch(selectedValues.get(i), 0, Position.Bias.Forward));
				}
			}
			
			// sort the modified list (the originating list should still be sorted)
			if( withSort )
				nLibrary.nInterface.sortModel(modeldestination);
			
			// get the new indices of the just-transferred values
			int[] newselectedindices = new int[selectedValues.size()];
			for(int i=0;i<selectedValues.size();i++) {
				// this gets the index of the item in its new list and saves it to the selected index global
				newselectedindices[i]=destination.getNextMatch(selectedValues.get(i), 0, Position.Bias.Forward);
			}
			
			// update the originanting list and clear the selection (if any)
			origin.updateUI();
			origin.clearSelection();
			
			// set the selection
			destination.updateUI();
			if( newselectedindices.length > 0 ) {
				destination.setSelectedIndices(newselectedindices);
				destination.ensureIndexIsVisible(destination.getSelectedIndex());
			}
		}
	}
	
	/**
	 * Sorts (alphabetically) a DefaultListModel&lt;String&gt;.
	 * @param model
	 */
	public static void sortModel(DefaultListModel<String> model) {
		try {
			List<String> list = new ArrayList<>();
			for (int i = 0; i < model.size(); i++) {
				list.add(model.get(i));  // copy the model to a list
			}
			Collections.sort(list);  // sort the list
			model.removeAllElements(); // remove all existing elements of the model
			for (String s : list) {
				model.addElement(s);  // ... and add back the sorted list
			}
		}
		catch(Exception e) {
			nLibrary.nCore.handleStackTrace(e);
		}
	}
	
	/** Refreshes the UI when the LookAndFeel has been changed.
	 * 
	 * @param frame
	 */
	public static void refreshLookAndFeel(JFrame frame) {
		SwingUtilities.updateComponentTreeUI(frame);
	}
	
	/**
	 * Sets a specified LookAndFeel.<br>
	 * A parameter of "default" sets the default LookAndFeel, as will any failure to set the LookAndFeel to something else.
	 * @param look - (String) a name of a LookAndFeel, or the default look and feel which is specified by the term held in the variable "systemDefaultTerm".
	 * @param frame - the frame to refresh when done
	 */
	public static void setLookAndFeel(String look, JFrame frame) {
		try {
			if( look.equals(systemDefaultTerm) ) {
				setDefaultLookAndFeel();
			}
			else {
		        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		            if (look.equals(info.getName())) {
		                UIManager.setLookAndFeel(info.getClassName());
		                break;
		            }
		        }
			}
	    } catch (Exception e) {
	    	setDefaultLookAndFeel();
	    }
		if( frame != null )
			refreshLookAndFeel(frame);
	}
	
	/**
	 * Sets the default LookAndFeel for the system.
	 */
	public static void setDefaultLookAndFeel() {
		try {
        	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception f) {
            f.printStackTrace();
        }
	}
	
	/**
	 * Returns a String array of the names of all the installed LookAndFeels.<br>
	 * @return String[] array
	 */
	public static String[] getInstalledLookAndFeels() {
		String[] lafs = new String[UIManager.getInstalledLookAndFeels().length];
		int n = 0;
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			lafs[n] = info.getName();
			n++;
        }
		return lafs;
	}
	
	/**
	 * Sets more optimal default interface settings.<br>
	 * Dismiss delay = 50 seconds<br>
	 * Reshow delay = 0 seconds<br>
	 * Initial delay = 1/10th of a second
	 */
	public static void setDefaultTooltipSettings() {
		setTooltipSettings(50000,0,100);
	}
	
	/**
	 * Sets the tooltip settings
	 * @param dismissDelay (int) milliseconds
	 * @param reshowDelay (int) milliseconds
	 * @param initialDelay (int) milliseconds
	 */
	public static void setTooltipSettings(int dismissDelay, int reshowDelay, int initialDelay) {
		ToolTipManager ttm = ToolTipManager.sharedInstance();
		ttm.setDismissDelay(dismissDelay);
		ttm.setReshowDelay(reshowDelay);
		ttm.setInitialDelay(initialDelay);
	}
	
	/**
	 * Shows a popup message.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 */
	public static void popupMessage(String message) {
		JOptionPane.showMessageDialog(null,message,"",JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Shows a popup message.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 */
	public static void popupMessage(String message, int messageType) {
		JOptionPane.showMessageDialog(null,message,"",messageType);
	}
	
	/**
	 * Shows a popup message.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 */
	public static void popupMessage(Component parentObj,String message) {
		JOptionPane.showMessageDialog(parentObj,message,null,JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Shows a popup message.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 */
	public static void popupMessage(String message, String title, int messageType) {
		JOptionPane.showMessageDialog(null,message,title,messageType);
	}
	
	/**
	 * Shows a popup message.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 */
	public static void popupMessage(Component parentObj,String message, int messageType) {
		JOptionPane.showMessageDialog(parentObj,message,null,messageType);
	}
	
	/**
	 * Shows a popup message.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 */
	public static void popupMessage(Component parentObj,String message, String title) {
		JOptionPane.showMessageDialog(parentObj,message,title,JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Shows a popup message.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 */
	public static void popupMessage(Component parentObj,String message, String title, int messageType) {
		JOptionPane.showMessageDialog(parentObj,message,title,messageType);
	}
	
	/**
	 * Creates a simple yes/no confirm prompt.<br>
	 * Returns true if YES, false if NO or the dialog was otherwise closed.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Defaults to JOptionPane.YES_NO_OPTION. Can only be one of:
	 * <ul><li>JOptionPane.DEFAULT_OPTION</li><li>JOptionPane.YES_NO_OPTION</li><li>JOptionPane.YES_NO_CANCEL_OPTION</li><li>JOptionPane.OK_CANCEL_OPTION</li></ul>
	 * @return boolean
	 */
	public static boolean popupConfirmation(String message) {
		return popupConfirmation(null,message,"Confirmation dialog", JOptionPane.YES_NO_OPTION);
	}
	
	/**
	 * Creates a simple yes/no confirm prompt.<br>
	 * Returns true if YES, false if NO or the dialog was otherwise closed.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Defaults to JOptionPane.YES_NO_OPTION. Can only be one of:
	 * <ul><li>JOptionPane.DEFAULT_OPTION</li><li>JOptionPane.YES_NO_OPTION</li><li>JOptionPane.YES_NO_CANCEL_OPTION</li><li>JOptionPane.OK_CANCEL_OPTION</li></ul>
	 * @return boolean
	 */
	public static boolean popupConfirmation(String message, String title) {
		return popupConfirmation(null,message,title, JOptionPane.YES_NO_OPTION);
	}
	
	/**
	 * Creates a simple yes/no confirm prompt.<br>
	 * Returns true if YES, false if NO or the dialog was otherwise closed.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Defaults to JOptionPane.YES_NO_OPTION. Can only be one of:
	 * <ul><li>JOptionPane.DEFAULT_OPTION</li><li>JOptionPane.YES_NO_OPTION</li><li>JOptionPane.YES_NO_CANCEL_OPTION</li><li>JOptionPane.OK_CANCEL_OPTION</li></ul>
	 * @return boolean
	 */
	public static boolean popupConfirmation(Component parentObj, String message, String title) {
		return popupConfirmation(parentObj,message,title, JOptionPane.YES_NO_OPTION);
	}
	
	/**
	 * Creates a simple yes/no confirm prompt.<br>
	 * Returns true if YES, false if NO or the dialog was otherwise closed.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Defaults to JOptionPane.YES_NO_OPTION. Can only be one of:
	 * <ul><li>JOptionPane.DEFAULT_OPTION</li><li>JOptionPane.YES_NO_OPTION</li><li>JOptionPane.YES_NO_CANCEL_OPTION</li><li>JOptionPane.OK_CANCEL_OPTION</li></ul>
	 * @return boolean
	 */
	public static boolean popupConfirmation(Component parentObj, String message, String title, int messageType) {
		int re = JOptionPane.showConfirmDialog(parentObj, message, title, messageType);  // returns 0 if yes, 1 if no, -1 if closed
    	if( re==0 )
    		return true;
		return false;
	}
	
	/**
	 * Shows a popup message with a textarea input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param title - the title
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupTextarea(String title) {
		return popupTextarea(null,title,JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Shows a popup message with a textarea input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param title - the title
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupTextarea(String title, int messageType) {
		return popupTextarea(null,title,messageType);
	}
	
	/**
	 * Shows a popup message with a textarea input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param title - the title
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupTextarea(Component parentObj, String title, int messageType) {
		final JTextArea textArea = new JTextArea();
		textArea.setEditable(true);
		final JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.requestFocus();
		textArea.requestFocusInWindow();
		scrollPane.setPreferredSize(new Dimension(400, 300));
		JOptionPane.showMessageDialog(parentObj, scrollPane, title, messageType);
		return textArea.getText();
	}
	
	/**
	 * Shows a popup message with a textfield input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupInput(String message) {
		return popupInput(null,message,null,JOptionPane.QUESTION_MESSAGE);
	}
	
	/**
	 * Shows a popup message with a textfield input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupInput(Component parentObj, String message) {
		return popupInput(parentObj,message,null,JOptionPane.QUESTION_MESSAGE);
	}
	
	/**
	 * Shows a popup message with a textfield input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupInput(String message, String title, int messageType) {
		return popupInput(null,message,title,messageType);
	}
	
	/**
	 * Shows a popup message with a textfield input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupInput(String message, String title) {
		return popupInput(null,message,title,JOptionPane.QUESTION_MESSAGE);
	}
	
	/**
	 * Shows a popup message with a textfield input. The input text is returned.
	 * @param parentObj - (optional) determines the Frame in which the dialog is displayed; if null,or if the parentComponent has no Frame, a default Frame is used
	 * @param message - the message to display in the title
	 * @param title - (optional) the title to use
	 * @param messageType - (optional) Shows an optional icon. Use one of:
	 * <ul><li>JOptionPane.PLAIN_MESSAGE</li><li>JOptionPane.INFORMATION_MESSAGE</li><li>JOptionPane.QUESTION_MESSAGE</li><li>JOptionPane.WARNING_MESSAGE</li><li>JOptionPane.ERROR_MESSAGE</li></ul>
	 * @return String
	 */
	public static String popupInput(Component parentObj, String message, String title, int messageType) {
		return JOptionPane.showInputDialog(parentObj, message, title, messageType);
	}
	
}
