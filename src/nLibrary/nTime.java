package nLibrary;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * A class for handling time.
 * @author Nathan Buckley
 *
 */
public class nTime {

	/**
	 * Given a long time value in milliseconds, returns a human readable value (a String) representing that value.<br>
	 * <b>If the amount of time supplied is less than:</b><br>
	 * - 1 second, it will return the value in milliseconds.<br>
	 * - 2 minutes, it will return the value converted to seconds, such as "110 seconds".<br>
	 * - 2 hours, it will return the value converted to minutes, such as "93 minutes"<br>
	 * - 2 days, it will return the value converted to hours, such as "18 hours"<br>
	 * - 2 weeks, it will return the value converted to days, such as "13 days"<br>
	 * - 2 months, it will return the value converted to weeks, such as "7 weeks"<br>
	 * - 2 years, it will return the value converted to months, such as "14 months"<br>
	 * - 2 centuries, it will return the value converted to years, such as "175 years"<br>
	 * - 1 millennium, it will return the value converted to centuries, such as "90 centuries"<br>
	 * Otherwise, it will return the value converted to millennia.<br>Sign (positive or negative) has no impact on the String returned.
	 * @param time - the time in milliseconds
	 * @param precision - (optional) The decimal precision you want to use. If omitted, defaults to 1.
	 * @return String
	 */
	final public static String getHumanReadableDurationFromMinutes(final long time) {
		return getHumanReadableDurationFromMilliseconds(time*1000*60,1);
	}
	
	/**
	 * Given a long time value in milliseconds, returns a human readable value (a String) representing that value.<br>
	 * <b>If the amount of time supplied is less than:</b><br>
	 * - 1 second, it will return the value in milliseconds.<br>
	 * - 2 minutes, it will return the value converted to seconds, such as "110 seconds".<br>
	 * - 2 hours, it will return the value converted to minutes, such as "93 minutes"<br>
	 * - 2 days, it will return the value converted to hours, such as "18 hours"<br>
	 * - 2 weeks, it will return the value converted to days, such as "13 days"<br>
	 * - 2 months, it will return the value converted to weeks, such as "7 weeks"<br>
	 * - 2 years, it will return the value converted to months, such as "14 months"<br>
	 * - 2 centuries, it will return the value converted to years, such as "175 years"<br>
	 * - 1 millennium, it will return the value converted to centuries, such as "90 centuries"<br>
	 * Otherwise, it will return the value converted to millennia.<br>Sign (positive or negative) has no impact on the String returned.
	 * @param time - the time in milliseconds
	 * @param precision - (optional) The decimal precision you want to use. If omitted, defaults to 1.
	 * @return String
	 */
	final public static String getHumanReadableDurationFromMinutes(final long time, final int precision) {
		return getHumanReadableDurationFromMilliseconds(time*1000*60,precision);
	}
	
	/**
	 * Given a long time value in milliseconds, returns a human readable value (a String) representing that value.<br>
	 * <b>If the amount of time supplied is less than:</b><br>
	 * - 1 second, it will return the value in milliseconds.<br>
	 * - 2 minutes, it will return the value converted to seconds, such as "110 seconds".<br>
	 * - 2 hours, it will return the value converted to minutes, such as "93 minutes"<br>
	 * - 2 days, it will return the value converted to hours, such as "18 hours"<br>
	 * - 2 weeks, it will return the value converted to days, such as "13 days"<br>
	 * - 2 months, it will return the value converted to weeks, such as "7 weeks"<br>
	 * - 2 years, it will return the value converted to months, such as "14 months"<br>
	 * - 2 centuries, it will return the value converted to years, such as "175 years"<br>
	 * - 1 millennium, it will return the value converted to centuries, such as "90 centuries"<br>
	 * Otherwise, it will return the value converted to millennia.<br>Sign (positive or negative) has no impact on the String returned.
	 * @param time - the time in milliseconds
	 * @param precision - (optional) The decimal precision you want to use. If omitted, defaults to 1.
	 * @return String
	 */
	final public static String getHumanReadableDurationFromSeconds(final long time) {
		return getHumanReadableDurationFromMilliseconds(time*1000,1);
	}
	
	/**
	 * Given a long time value in milliseconds, returns a human readable value (a String) representing that value.<br>
	 * <b>If the amount of time supplied is less than:</b><br>
	 * - 1 second, it will return the value in milliseconds.<br>
	 * - 2 minutes, it will return the value converted to seconds, such as "110 seconds".<br>
	 * - 2 hours, it will return the value converted to minutes, such as "93 minutes"<br>
	 * - 2 days, it will return the value converted to hours, such as "18 hours"<br>
	 * - 2 weeks, it will return the value converted to days, such as "13 days"<br>
	 * - 2 months, it will return the value converted to weeks, such as "7 weeks"<br>
	 * - 2 years, it will return the value converted to months, such as "14 months"<br>
	 * - 2 centuries, it will return the value converted to years, such as "175 years"<br>
	 * - 1 millennium, it will return the value converted to centuries, such as "90 centuries"<br>
	 * Otherwise, it will return the value converted to millennia.<br>Sign (positive or negative) has no impact on the String returned.
	 * @param time - the time in milliseconds
	 * @param precision - (optional) The decimal precision you want to use. If omitted, defaults to 1.
	 * @return String
	 */
	final public static String getHumanReadableDurationFromSeconds(final long time, final int precision) {
		return getHumanReadableDurationFromMilliseconds(time*1000,precision);
	}
	
	/**
	 * Given a long time value in milliseconds, returns a human readable value (a String) representing that value.<br>
	 * <b>If the amount of time supplied is less than:</b><br>
	 * - 1 second, it will return the value in milliseconds.<br>
	 * - 2 minutes, it will return the value converted to seconds, such as "110 seconds".<br>
	 * - 2 hours, it will return the value converted to minutes, such as "93 minutes"<br>
	 * - 2 days, it will return the value converted to hours, such as "18 hours"<br>
	 * - 2 weeks, it will return the value converted to days, such as "13 days"<br>
	 * - 2 months, it will return the value converted to weeks, such as "7 weeks"<br>
	 * - 2 years, it will return the value converted to months, such as "14 months"<br>
	 * - 2 centuries, it will return the value converted to years, such as "175 years"<br>
	 * - 1 millennium, it will return the value converted to centuries, such as "90 centuries"<br>
	 * Otherwise, it will return the value converted to millennia.<br>Sign (positive or negative) has no impact on the String returned.
	 * @param time - the time in milliseconds
	 * @param precision - (optional) The decimal precision you want to use. If omitted, defaults to 1.
	 * @return String
	 */
	final public static String getHumanReadableDurationFromMilliseconds(final long time) {
		return getHumanReadableDurationFromMilliseconds(time,1);
	}
	
	/**
	 * Given a long time value in milliseconds, returns a human readable value (a String) representing that value.<br>
	 * <b>If the amount of time supplied is less than:</b><br>
	 * - 1 second, it will return the value in milliseconds.<br>
	 * - 2 minutes, it will return the value converted to seconds, such as "110 seconds".<br>
	 * - 2 hours, it will return the value converted to minutes, such as "93 minutes"<br>
	 * - 2 days, it will return the value converted to hours, such as "18 hours"<br>
	 * - 2 weeks, it will return the value converted to days, such as "13 days"<br>
	 * - 2 months, it will return the value converted to weeks, such as "7 weeks"<br>
	 * - 2 years, it will return the value converted to months, such as "14 months"<br>
	 * - 2 centuries, it will return the value converted to years, such as "175 years"<br>
	 * - 1 millennium, it will return the value converted to centuries, such as "90 centuries"<br>
	 * Otherwise, it will return the value converted to millennia.<br>Sign (positive or negative) has no impact on the String returned.
	 * @param time - the time in milliseconds
	 * @param precision - (optional) The decimal precision you want to use. If omitted, defaults to 1.
	 * @return String
	 */
	final public static String getHumanReadableDurationFromMilliseconds(final long time, final int precision) {
		if( Math.abs(time) < 1000 )
			return time + " milliseconds";
		
		final double milliseconds = (double) Math.abs(time);
		final double seconds = milliseconds/1000D;
		if( seconds < 120 ) {
			// nMath.round() actually uses Math.round() if the precision is 0, but it returns a double so the time value String will end up with ".0" at the end
			// so we'll just check and use the long Math version if we aren't using decimals at all
			if( precision == 0 )
				return Math.round(seconds) + " seconds";
			else
				return nMath.round(seconds,precision) + " seconds";
		}
		
		final double minutes = seconds/60D;
		if( minutes < 120 ) {
			if( precision == 0 )
				return Math.round(minutes) + " minutes";
			else
				return nMath.round(minutes,precision) + " minutes";
		}
		
		final double hours = minutes/60D;
		if( hours < 48 ) {
			if( precision == 0 )
				return Math.round(hours) + " hours";
			else
				return nMath.round(hours,precision) + " hours";
		}
		
		final double days = hours/24D;
		if( days < 14 ) {
			if( precision == 0 )
				return Math.round(days) + " days";
			else
				return nMath.round(days,precision) + " days";
		}
		
		final double weeks = days/7D;
		if( weeks < 8 ) {
			if( precision == 0 )
				return Math.round(weeks) + " weeks";
			else
				return nMath.round(weeks,precision) + " weeks";
		}
		
		final double months = weeks/4; // approximate, since the number of days in a month varies
		if( months < 24 ) {
			if( precision == 0 )
				return Math.round(months) + " months";
			else
				return nMath.round(months,precision) + " months";
		}
		
		final double years = months/12D;
		if( years < 200 ) {
			if( precision == 0 )
				return Math.round(years) + " years";
			else
				return nMath.round(years,precision) + " years";
		}
		
		final double centuries = years/10D;
		if( centuries < 20 ) {
			if( precision == 0 )
				return Math.round(centuries) + " centuries";
			else
				return nMath.round(centuries,precision) + " centuries";
		}
		
		final double millennia = centuries/10D;
		if( precision == 0 )
			return Math.round(millennia) + " millennia";
		else
			return nMath.round(millennia,precision) + " millennia";
	}
	
	/**
	 * Assuming the start time has been set [nCore.startTime], then this method returns the elapsed time in seconds since then. You must set the start time before calling this for it to return a useful value.
	 * @param timeMs (optional) - if omitted, uses the current system time.
	 * @return int elapsed time in seconds since the start time
	 */
	public static int getElapsedTime() {
		return getElapsedTime(System.currentTimeMillis());
	}
	
	/**
	 * Assuming the start time has been set [nCore.startTime], then this method returns the elapsed time in seconds since then. You must set the start time before calling this for it to return a useful value.
	 * @param timeMs (optional) - if omitted, uses the current system time.
	 * @return int elapsed time in seconds since the start time
	 */
	public static int getElapsedTime(long timeMs) {
		return (int)Math.floor((timeMs-nCore.startTime)/1000D);
	}
	
	/** Given two times in the format 'HH:mm:ss' or 'HHmm', return the difference in minutes.<br>
	 * Because it uses java.util.Timezone.parse, it has a chance to be wrong about day. Recommend using getDateDifference() instead if you have the date objects.<br>
	 * Returns <b>-1</b> if invalid (the time difference should otherwise always be >= 0).
	 * 
	 * @param startTime - the start time
	 * @param endTime - the end time
	 * @param timeUnit - (optional) the units you want the result in. Defaults to minutes.
	 * @return a long value in the units specified
	 */
	public static long getHHmmTimeDifference(String startTime, String endTime) {
		return getHHmmTimeDifference(startTime, endTime, TimeUnit.MINUTES);
	}
	
	/** Given two times in the format 'HH:mm:ss' or 'HHmm', return the difference in minutes.<br>
	 * Because it uses java.util.Timezone.parse, it has a chance to be wrong about day. Recommend using getDateDifference() instead if you have the date objects (it's also much faster).<br>
	 * Returns <b>-1</b> if invalid (the time difference should otherwise always be >= 0).
	 * 
	 * @param startTime - the start time
	 * @param endTime - the end time
	 * @param timeUnit - (optional) the units you want the result in. Defaults to minutes.
	 * @return a long value in the units specified
	 */
	public static long getHHmmTimeDifference(String startTime, String endTime, TimeUnit timeUnit) {
		try {
			if( startTime.contains(":") ) {
				startTime = startTime.replace(":","");
				if(startTime.length()>4)
					startTime=startTime.substring(0, 4);
			}
			if( endTime.contains(":") ) {
				endTime = endTime.replace(":","");
				if(endTime.length()>4)
					endTime=endTime.substring(0, 4);
			}
			if(endTime.length()!=4) {
				nLibrary.nCore.print("Invalid time passed to getTimeDifference(): "+endTime);
				return -1;
			}
			if(startTime.length()!=4) {
				nLibrary.nCore.print("Invalid time passed to getTimeDifference(): "+startTime);
				return -1;
			}
			Date startDate = null;
			Date endDate = null;
			try {
				SimpleDateFormat HHmm = new SimpleDateFormat("HHmm");
				HHmm.setTimeZone(TimeZone.getTimeZone("UTC"));
				startDate = HHmm.parse(startTime);
				endDate = HHmm.parse(endTime);
			} catch (ParseException e) {
				nLibrary.nCore.handleStackTrace(e);
				return -1;
			}
	
			// if there was a problem with converting either, return null
			if( startDate == null || endDate == null )
				return -1;
			
			long diffMs  = endDate.getTime() - startDate.getTime();
			return timeUnit.convert(diffMs,TimeUnit.MILLISECONDS);

		}
		catch(Exception e) {
			nLibrary.nCore.handleStackTrace(e);
			return -1;
		}
	}
	
	/**
	 * Get the difference in time between two dates.
	 * @param startDate - the oldest date (if this is the oldest date and endDate is the newest date, this function will return a negative value)
	 * @param endDate - the newest date (if this is the oldest date and StartDate is the newest date, this function will return a positive value)
	 * @param timeUnit - (optional) The TimeUnit in which you want the return value (e.g. TimeUnit.MILLISECONDS). If omitted, defaults to minutes.
	 * @return the difference value in the units specified by timeUnit
	 */
	public static Long getDateDifference(Date startDate, Date endDate) {
		return getDateDifference(startDate,endDate,TimeUnit.MINUTES);
	}
	
	/**
	 * Get the difference in time between two dates.
	 * @param startDate - the oldest date (if this is the oldest date and endDate is the newest date, this function will return a negative value)
	 * @param endDate - the newest date (if this is the oldest date and StartDate is the newest date, this function will return a positive value)
	 * @param timeUnit - (optional) The TimeUnit in which you want the return value (e.g. TimeUnit.MILLISECONDS). If omitted, defaults to minutes.
	 * @return the difference value in the units specified by timeUnit
	 */
	public static Long getDateDifference(Long startDate, Long endDate) {
		return getDateDifference(startDate,endDate,TimeUnit.MINUTES);
	}
	
	/**
	 * Get the difference in time between two dates.
	 * @param startDate - the oldest date (if this is the oldest date and endDate is the newest date, this function will return a negative value)
	 * @param endDate - the newest date (if this is the oldest date and StartDate is the newest date, this function will return a positive value)
	 * @param timeUnit - (optional) The TimeUnit in which you want the return value (e.g. TimeUnit.MILLISECONDS). If omitted, defaults to minutes.
	 * @return the difference value in the units specified by timeUnit
	 */
	public static Long getDateDifference(Date startDate, Date endDate, TimeUnit timeUnit) {
		if( endDate==null || startDate==null )
			return null;
	    if(timeUnit==null)
	    	timeUnit = TimeUnit.MINUTES;
	    long diffMS = endDate.getTime() - startDate.getTime();
	    return timeUnit.convert(diffMS,TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Get the difference in time between two dates.
	 * @param startDate - the oldest date (if this is the oldest date and endDate is the newest date, this function will return a negative value)
	 * @param endDate - the newest date (if this is the oldest date and StartDate is the newest date, this function will return a positive value)
	 * @param timeUnit - (optional) The TimeUnit in which you want the return value (e.g. TimeUnit.MILLISECONDS). If omitted, defaults to minutes.
	 * @return the difference value in the units specified by timeUnit
	 */
	public static Long getDateDifference(Long startDate, Long endDate, TimeUnit timeUnit) {
		if( startDate==null || endDate==null )
			return null;
	    if(timeUnit==null)
	    	timeUnit = TimeUnit.MINUTES;
		long diffMS = startDate - endDate;
	    return timeUnit.convert(diffMS,TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Converts a time from one unit to another.
	 * @param value
	 * @param originalUnit
	 * @param desiredUnit
	 * @return
	 */
	public static Long timeConversion(long value, TimeUnit originalUnit, TimeUnit desiredUnit) {
		if( originalUnit==null || desiredUnit==null )
			return null;
		return desiredUnit.convert(value, originalUnit);
	}
	
	/**
	 * Given a time in 'HH:mm:ss' or 'HHmm' format, and a number of minutes (positive or negative), get the new time that the combination of those times would result.<br>
	 * Returns <b>null</b> if unable to parse the input time.
	 * @param time
	 * @param minutes
	 * @return
	 */
	public static String getModifiedTime(String time, int minutes) {
		if( time.contains(":") ) {
			time = time.replace(":","");
			if(time.length()>4)
				time=time.substring(0, 4);
		}
		if(time.length()!=4) {
			nLibrary.nCore.print("Invalid time passed to getModifiedTime(): "+time);
			return null;
		}
		//time = time.substring(0, 2)+":"+time.substring(2);  // time should be in the format 'HH:mm'
		Date d = null;
		SimpleDateFormat HHmm = new SimpleDateFormat("HHmm");
		HHmm.setTimeZone(TimeZone.getTimeZone("GMT"));
		try { d = HHmm.parse(time); } catch (ParseException e) { nLibrary.nCore.handleStackTrace(e); }
		if( d==null )
			return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.MINUTE,minutes);
		return HHmm.format(cal.getTime());
	}
	
	/**
	 * Checks if the given time looks like it is a valid time value in the format provided.
	 * @param time
	 * @return
	 */
	public static boolean isValidTimeString(String time, String format) {
		if( time == null || time.equals("-") || time.trim().equals("") || time.length()!=format.length() )
			return false;
		DateFormat dformat = new SimpleDateFormat(format);
		dformat.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			dformat.parse(time);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if the input String is a valid epoch time.
	 * @param epochTime
	 * @return true or false
	 */
	public static boolean isValidEpochTime(String epochTime) {
		if( epochTime == null || epochTime.equals("-") || epochTime.trim().equals("") )
			return false;
		return isValidEpochTime(Long.parseLong(epochTime));
	}
	
	/**
	 * Checks if the input Long is a valid epoch time.
	 * @param epochTime
	 * @return true or false
	 */
	public static boolean isValidEpochTime(Long epochTime) {
		try {
			new Date(epochTime);
		} catch(Exception e) {
			return false;
		}
		return true;
	}
	
	
	
	/**
	 * Returns the 4-character time with preceding zeros as needed based on an integer input.
	 * @param time
	 * @return
	 */
	public static String getFourDigitTimeFromInt(int time) {
		if( time < 10 )
			return "000"+String.valueOf(time); // times like 0005, 0004, 0009 which as ints will drop the preceding 3 zeros
		else if( time < 100 )
			return "00"+String.valueOf(time); // times like 0015, 0024, 0059 which as ints will drop the preceding 2 zeros
		else if( time < 1000 )
			return "0"+String.valueOf(time);  // times like 0215, 0724, 0959 which as ints will drop the preceding zero
		else
			return String.valueOf(time);
	}
	
	/** Returns the date in String format.<br>
	 * 
	 * @param d - a date object, or if omitted defaults to current date
	 * @param format - format, such as "HH:mm:ss". If omitted, defaults to 'dd/MM/yyyy HH:mm:ss'
	 * @param timezone - sets the timezone of the date. If omitted, defaults to UTC.
	 * @return formatted date String
	 */
	public static String getStringFromDate() {
		return getStringFromDate(new Date());
	}
	
	/** Returns the date in String format.<br>
	 * 
	 * @param d - (optional) a date object, or if omitted defaults to current date
	 * @param format - (optional) format, such as "HH:mm:ss". If omitted, defaults to 'dd/MM/yyyy HH:mm:ss'
	 * @param timezone - (optional) sets the timezone of the date. If omitted, defaults to UTC.
	 * @return formatted (optional) date String
	 */
	public static String getStringFromDate(Date d) {
		return getStringFromDate(d,"dd/MM/yyyy HH:mm:ss","UTC");
	}
	
	/** Returns the date in String format.<br>
	 * 
	 * @param d - a date object, or if omitted defaults to current date
	 * @param format - format, such as "HH:mm:ss". If omitted, defaults to 'dd/MM/yyyy HH:mm:ss'
	 * @param timezone - sets the timezone of the date. If omitted, defaults to UTC.
	 * @return formatted date String
	 */
	public static String getStringFromDate(Date d, String format) {
		final SimpleDateFormat ddMMyyyyHHmmss = new SimpleDateFormat(format);
		ddMMyyyyHHmmss.setTimeZone(TimeZone.getTimeZone("UTC"));
		return ddMMyyyyHHmmss.format(d);
	}
	
	/** Returns the date in String format.<br>
	 * 
	 * @param d - a date object, or if omitted defaults to current date
	 * @param format - format, such as "HH:mm:ss". If omitted, defaults to 'dd/MM/yyyy HH:mm:ss'
	 * @param timezone - sets the timezone of the date. If omitted, defaults to UTC.
	 * @return formatted date String
	 */
	public static String getStringFromDate(String format) {
		final SimpleDateFormat ddMMyyyyHHmmss = new SimpleDateFormat(format);
		ddMMyyyyHHmmss.setTimeZone(TimeZone.getTimeZone("UTC"));
		return ddMMyyyyHHmmss.format(new Date());
	}
	
	/** Returns the date in String format.<br>
	 * 
	 * @param d - a date object, or if omitted defaults to current date
	 * @param format - format, such as "HH:mm:ss". If omitted, defaults to 'dd/MM/yyyy HH:mm:ss'
	 * @param timezone - sets the timezone of the date. If omitted, defaults to UTC.
	 * @return formatted date String
	 */
	public static String getStringFromDate(String format, String timezone) {
		final SimpleDateFormat ddMMyyyyHHmmss = new SimpleDateFormat(format);
		ddMMyyyyHHmmss.setTimeZone(TimeZone.getTimeZone(timezone));
		return ddMMyyyyHHmmss.format(new Date());
	}
	
	/** Returns the date in String format.<br>
	 * 
	 * @param d - a date object, or if omitted defaults to current date
	 * @param format - format, such as "HH:mm:ss". If omitted, defaults to 'dd/MM/yyyy HH:mm:ss'
	 * @param timezone - sets the timezone of the date. If omitted, defaults to UTC.
	 * @return formatted date String
	 */
	public static String getStringFromDate(Date d, String format, String timezone) {
		final SimpleDateFormat ddMMyyyyHHmmss = new SimpleDateFormat(format);
		ddMMyyyyHHmmss.setTimeZone(TimeZone.getTimeZone(timezone));
		return ddMMyyyyHHmmss.format(d);
	}
	
	/**
	 * Returns a date String for the given epoch time in the given format.
	 * @param epochtime - a epoch time String
	 * @param format - (optional) A SimpleDateFormat for the returned time String. If omitted, defaults to "dd/MM/yyyy HH:mm:ss".
	 * @param timezone - (optional) time zone of the input date. If omitted, defaults to "UTC".
	 * @return A date String in the format requested for the given epoch time in the format requested
	 */
	public static String getStringFromEpochString(String epochtime, String format) {
		return getStringFromEpochString(epochtime,format,"UTC");
	}
	
	/**
	 * Returns a date String for the given epoch time in the given format.
	 * @param epochtime - a epoch time String
	 * @param format - (optional) A SimpleDateFormat for the returned time String. If omitted, defaults to "dd/MM/yyyy HH:mm:ss".
	 * @param timezone - (optional) time zone of the input date. If omitted, defaults to "UTC".
	 * @return A date String in the format requested for the given epoch time in the format requested
	 */
	public static String getStringFromEpochString(String epochtime, String format, String timezone) {
		if( epochtime == null || format == null || timezone == null )
			return null;
		final Date d = getDateFromEpochString(epochtime);
		if( d!=null ) {
			final SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.setTimeZone(TimeZone.getTimeZone(timezone));
			return sdf.format(d);
		}
		else
			return null;
	}

	/**
	 * Returns a date String for the given epoch time in the given format.
	 * @param epochtime - a Long epoch time
	 * @param format - (optional) A SimpleDateFormat for the returned time String. If omitted, defaults to "dd/MM/yyyy HH:mm:ss".
	 * @param timezone - (optional) time zone of the input date. If omitted, defaults to "UTC".
	 * @return A date String in the format requested for the given epoch time in the format requested
	 */
	public static String getStringFromLong(Long epochtime) {
		return getStringFromLong(epochtime,"dd/MM/yyyy HH:mm:ss","UTC");
	}
	
	/**
	 * Returns a date String for the given epoch time in the given format.
	 * @param epochtime - a Long epoch time
	 * @param format - (optional) A SimpleDateFormat for the returned time String. If omitted, defaults to "dd/MM/yyyy HH:mm:ss".
	 * @param timezone - (optional) time zone of the input date. If omitted, defaults to "UTC".
	 * @return A date String in the format requested for the given epoch time in the format requested
	 */
	public static String getStringFromLong(Long epochtime, String format) {
		return getStringFromLong(epochtime,format,"UTC");
	}
	
	/**
	 * Returns a date String for the given epoch time in the given format.
	 * @param epochtime - a Long epoch time
	 * @param format - (optional) A SimpleDateFormat for the returned time String. If omitted, defaults to "dd/MM/yyyy HH:mm:ss".
	 * @param timezone - (optional) time zone of the input date. If omitted, defaults to "UTC".
	 * @return A date String in the format requested for the given epoch time in the format requested
	 */
	public static String getStringFromLong(Long epochtime, String format, String timezone) {
		if( epochtime == null || format == null || timezone == null )
			return null;
		final Date d = getDateFromLong(epochtime);
		if( d!=null ) {
			final SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.setTimeZone(TimeZone.getTimeZone(timezone));
			return sdf.format(d);
		}
		else
			return null;
	}
	
	/**
	 * Gets the epoch String from the specified date.
	 * @param d - (optional) the Date object. If omitted, uses the current date. 
	 * @return An epoch time in String format
	 */
	public static String getEpochStringFromDate() {
		return String.valueOf(System.currentTimeMillis());
	}
	
	/**
	 * Gets the epoch String from the specified date.
	 * @param d - (optional) the Date object. If omitted, uses the current date. 
	 * @return An epoch time in String format
	 */
	public static String getEpochStringFromDate(Date d) {
		if(d==null)
			return null;
		return String.valueOf(d.getTime());
	}
	
	/**
	 * Gets the epoch long from the specified date.
	 * @param d - (optional) the Date object. If omitted, uses the current date.
	 * @return
	 */
	public static long getLongFromDate() {
		return System.currentTimeMillis();
	}
	
	/**
	 * Gets the epoch long from the specified date.
	 * @param d - (optional) the Date object. If omitted, uses the current date.
	 * @return
	 */
	public static long getLongFromDate(Date d) {
		return d.getTime();
	}
	
	
	/** Returns a Date object from a given epoch time string.
	 * 
	 * @param epoch
	 * @return
	 */
	public static Date getDateFromEpochString(String epochtime) {
		if( epochtime == null )
			return null;
		try {
			return new Date(Long.parseLong(epochtime));
		} catch(NumberFormatException nfe) {
			return null;
		}
	}
	
	/** Returns a Date object from a given epoch Long.
	 * 
	 * @param epoch
	 * @return
	 */
	public static Date getDateFromLong(Long epochtime) {
		if( epochtime == null )
			return null;
		try {
			return new Date(epochtime);
		} catch(NumberFormatException nfe) {
			return null;
		}
	}
	
	/**
	 * Returns a UTC Date for given time String in the provided format.<br>
	 * @param date - the date String
	 * @param format - (optional) the SimpleDateFormat of the supplied data String. Defaults to 'ddMMyyyyHHmmss'.
	 * @param timezone - (optional) the timezone of the date. Defaults to UTC.
	 * @return A Date object at the given String date
	 */
	public static Date getDateFromString(String date) {
		return getDateFromString(date,"ddMMyyyyHHmmss","UTC");
	}
	
	/**
	 * Returns a UTC Date for given time String in the provided format.<br>
	 * @param dateStr - the date String
	 * @param format - (optional) the SimpleDateFormat of the supplied data String. Defaults to 'ddMMyyyyHHmmss'.
	 * @param timezone - (optional) the timezone of the date. Defaults to UTC.
	 * @return A Date object at the given String date
	 */
	public static Date getDateFromString(String dateStr, String format) {
		return getDateFromString(dateStr,format,"UTC");
	}
	
	/**
	 * Returns a Date object for given time String in the provided format.<br>
	 * @param date - the date String
	 * @param format - (optional) the SimpleDateFormat of the supplied data String. Defaults to 'ddMMyyyyHHmmss'.
	 * @param timezone - (optional) the timezone of the date. Defaults to UTC.
	 * @return A Date object at the given String date
	 */
	final public static Date getDateFromString(final String dateStr, final String format, final String timezone) {
		return getDateFromString(dateStr,format,TimeZone.getTimeZone(timezone));
	}
	
	/**
	 * Returns a Date object for given time String in the provided format.<br>
	 * @param date - the date String
	 * @param format - (optional) the SimpleDateFormat of the supplied data String. Defaults to 'ddMMyyyyHHmmss'.
	 * @param timezone - (optional) the timezone of the date. Defaults to UTC.
	 * @return A Date object at the given String date
	 */
	final public static Date getDateFromString(final String dateStr, final String format, final TimeZone timezone) {
		if( dateStr == null || format == null || timezone == null )
			return null;
		final DateFormat dformat = new SimpleDateFormat(format);
		dformat.setTimeZone(timezone);
		Date actualdate = null;
		try {
			actualdate = dformat.parse(dateStr);
		} catch (ParseException e) {
			nCore.print("nTime.getDateFromString(): Unable to parse string \""+dateStr+"\" using format \""+format+"\".","DEBUG");
			return null;
		}
		return actualdate;
	}
	
	/**
	 * Returns a ZonedDateTime from a given String.
	 * @param time - the String time to parse
	 * @param format - (optional) If omitted, defaults to "ddMMyyyyHHmmss".
	 * @param timezone - (optional) If omitted, defaults "[Europe/London]".
	 * @return
	 */
	public static ZonedDateTime getZonedDateTimeFromString(String time) {
		return getZonedDateTimeFromString(time,"ddMMyyyyHHmmss","UTC");
	}
	
	/**
	 * Returns a ZonedDateTime from a given String.
	 * @param time - the String time to parse
	 * @param format - (optional) If omitted, defaults to "ddMMyyyyHHmmss".
	 * @param timezone - (optional) If omitted, defaults "UTC".
	 * @return
	 */
	public static ZonedDateTime getZonedDateTimeFromString(String time, String format) {
		return getZonedDateTimeFromString(time,format,"UTC");
	}
	
	/**
	 * Returns a ZonedDateTime from a given String.
	 * @param time - the String time to parse.
	 * @param format - (optional) If omitted, defaults to "ddMMyyyyHHmmss".
	 * @param timezone - (optional) If omitted, defaults "UTC".
	 * @return
	 */
	public static ZonedDateTime getZonedDateTimeFromString(String time, String format, String timezone) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, Locale.ENGLISH);
		return ZonedDateTime.parse(time, formatter.withZone(ZoneId.of(timezone)));
	}
	
	/**
	 * Return a human readable time value of the given ZonedDateTime in the indicated format.
	 * @param zdt
	 * @param format - (optional) If omitted, defaults to "ddMMyyyyHHmmss".
	 * @return
	 */
	public static String getStringFromZonedDateTime(ZonedDateTime zdt) {
		return getStringFromZonedDateTime(zdt,"ddMMyyyyHHmmss");
	}
	
	/**
	 * Return a human readable time value of the given ZonedDateTime in the indicated format.
	 * @param zdt
	 * @param format - (optional) If omitted, defaults to "ddMMyyyyHHmmss".
	 * @return
	 */
	public static String getStringFromZonedDateTime(ZonedDateTime zdt, String format) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return zdt.format(formatter); 
	}
	
	/**
	 * Returns a Date object based on a given time string. <b>This is a fixed, non-relative time.</b> Usually used to parse <b>HHmm</b> or <b>HH:mm:ss</b> times.<br>
	 * This varies from nTime.getDateFromString() in that it will check to make sure the date falls within a 12 hour 
	 * window of the current time, because when parsing times without a day indicated the parser must "guess" a day (presumably 
	 * the current day) which could wrong if the time actually supplied referred to a past day.<br>
	 * If the Date object it generates is thus more than 12 hours away in any direction, it will adjust the date 24 hours back or
	 * forward as appropriate.
	 * @param format - (optional) the SimpleDateFormat of the supplied data String. Defaults to 'ddMMyyyyHHmmss'.
	 * @param timezone - (optional) the timezone of the date. Defaults to UTC.
	 * @param dateKnowledge - (optional) Information we know about the date. If omitted, defaults to 0.<br>
	 * 0 = it must be a future date<br>
	 * 1 = it must be a future date with the exception of any time less than 1 hour ago<br>
	 * 2 = it must be a past date<br>
	 * @return A Date object at the given String date
	 */
	public static Date getAdjustedDateFromString(String dateStr) {
		return getAdjustedDateFromString(dateStr,"ddMMyyyyHHmmss","UTC",0);
	}
	
	/** Returns a Date object based on a given time string. <b>This is a fixed, non-relative time.</b> Usually used to parse <b>HHmm</b> or <b>HH:mm:ss</b> times.<br>
	 * This varies from nTime.getDateFromString() in that it will check to make sure the date falls within a 12 hour 
	 * window of the current time, because when parsing times without a day indicated the parser must "guess" a day (presumably 
	 * the current day) which could wrong if the time actually supplied referred to a past day.<br>
	 * If the Date object it generates is thus more than 12 hours away in any direction, it will adjust the date 24 hours back or
	 * forward as appropriate.
	 * @param format - (optional) the SimpleDateFormat of the supplied data String. Defaults to 'ddMMyyyyHHmmss'.
	 * @param timezone - (optional) the timezone of the date. Defaults to UTC.
	 * @param dateKnowledge - (optional) Information we know about the date. If omitted, defaults to 0.<br>
	 * 0 = it must be a future date<br>
	 * 1 = it must be a future date with the exception of any time less than 1 hour ago<br>
	 * 2 = it must be a past date<br>
	 * @return A Date object at the given String date
	 */
	public static Date getAdjustedDateFromString(String dateStr, String format) {
		return getAdjustedDateFromString(dateStr,format,"UTC",0);
	}
	
	/** Returns a Date object based on a given time string. <b>This is a fixed, non-relative time.</b> Usually used to parse <b>HHmm</b> or <b>HH:mm:ss</b> times.<br>
	 * This varies from nTime.getDateFromString() in that it will check to make sure the date falls within a 12 hour 
	 * window of the current time, because when parsing times without a day indicated the parser must "guess" a day (presumably 
	 * the current day) which could wrong if the time actually supplied referred to a past day.<br>
	 * If the Date object it generates is thus more than 12 hours away in any direction, it will adjust the date 24 hours back or
	 * forward as appropriate.
	 * @param format - (optional) the SimpleDateFormat of the supplied data String. Defaults to 'ddMMyyyyHHmmss'.
	 * @param timezone - (optional) the timezone of the date. Defaults to UTC.
	 * @param dateKnowledge - (optional) Information we know about the date. If omitted, defaults to 0.<br>
	 * 0 = it must be a future date<br>
	 * 1 = it must be a future date with the exception of any time less than 1 hour ago<br>
	 * 2 = it must be a past date<br>
	 * @return A Date object at the given String date
	 */
	public static Date getAdjustedDateFromString(String dateStr, String format, String timezone, Integer dateKnowledge) {
		if( dateStr == null || dateStr.equals("") ) {
			nLibrary.nCore.print("nTime.getAdjustedDateFromString(): Unparseable date (null or empty string).","WARNING");
			return null;
		}
		if( format == null || format.equals("") ) {
			nLibrary.nCore.print("nTime.getAdjustedDateFromString(): Invalid format (null or empty string).","WARNING");
			return null;
		}
		DateFormat dformat = new SimpleDateFormat(format);
		if( timezone != null && !timezone.equals("") )
			dformat.setTimeZone(TimeZone.getTimeZone(timezone));
		Date actualDate = null;
		try {
			actualDate = dformat.parse(dateStr);
		} catch (ParseException e) {
			// TODO this doesn't work, seems to be caught by some other thing handling the stack trace
			nCore.handleStackTrace(e);
			return null;
		}
		
		// if there's no day/month/year specified in the time string, it will assume Jan 1st, 1970.
		// So we need to set it to today's date (for now) and then check if it's within a 12-hour window and adjust accordingly
		final long currentEpochLong = System.currentTimeMillis();
		
		// check if there's any date knowledge
		if( dateKnowledge == null || dateKnowledge.intValue() < 1 || dateKnowledge.intValue() > 2 ) {
			// it must be a future date
			if( actualDate.getTime() < currentEpochLong ) {
				// keep adding days until we're in the future
				long newDate = actualDate.getTime();
				while( newDate < currentEpochLong ) {
					newDate = nTime.addTime(currentEpochLong, 24, Calendar.HOUR);
				}
				// now just set actualDate to whatever newDate is
				actualDate.setTime(newDate);
			}
			
			// otherwise, for all we know we have the right date
			return actualDate;
		}
		else if( dateKnowledge.intValue() == 1 ) {
			// it must be a future date or within 1 hour ago
			if( actualDate.getTime() < nTime.addTime(currentEpochLong,-1,Calendar.HOUR) ) {
				// keep adding days until we're in the future
				long newDate = actualDate.getTime();
				while( newDate < nTime.addTime(currentEpochLong,-1,Calendar.HOUR) ) {
					newDate = nTime.addTime(currentEpochLong, 24, Calendar.HOUR);
				}
				actualDate.setTime(newDate);
			}
			
			// otherwise, for all we know we have the right date
			return actualDate;
		}
		else if( dateKnowledge.intValue() == 2 ) {
			// it must be a past date
			if( actualDate.getTime() > currentEpochLong ) {
				// keep subtracting days until we're in the past
				long newDate = actualDate.getTime();
				while( newDate > currentEpochLong ) {
					newDate = nTime.addTime(currentEpochLong, -24, Calendar.HOUR);
				}
				actualDate.setTime(newDate);
			}
			
			// otherwise, for all we know we have the right date
			return actualDate;
		}
		else
			return null;
	}
	
	/**
	 * Returns a long representing an HHmm time. Assumes the HHmm time is in the future and UTC time.
	 * @param HHmmTime
	 * @return
	 */
	public static long getLongFromHHmm(String HHmmTime) {
		// So we need to set it to today's date and set the hours and minutes to that which is supplied.
		Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		
		time.set(Calendar.HOUR_OF_DAY, Integer.parseInt(HHmmTime.substring(0,2)));
		time.set(Calendar.MINUTE, Integer.parseInt(HHmmTime.substring(2,4)));
		time.set(Calendar.SECOND, 0);
		time.set(Calendar.MILLISECOND, 0);
		
		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		
		// basically we're assuming that if the time is more than 1 hour in the past then we need to add 24 hrs so it is in the future
		// (max could be one just short of 1 hour if we are at say 0059 but looking at a full hour which starts at 0000)
		now.add(Calendar.HOUR, -1);
		if( time.before(now) )
			time.add(Calendar.HOUR, 24);

		return time.getTimeInMillis();
	}
	
	/**
	 * Returns a long representing an dd/HHmm time. Assumes the dd/HHmm time is in the future and UTC time.
	 * @param HHmmTime
	 * @return
	 */
	public static long getLongFromddHHmm(String ddHHmmTime) {
		// So we need to set it to today's date and set the hours and minutes to that which is supplied.
		Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		
		time.set(Calendar.DAY_OF_MONTH, Integer.parseInt(ddHHmmTime.substring(0,2)));
		time.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ddHHmmTime.substring(3,5)));
		time.set(Calendar.MINUTE, Integer.parseInt(ddHHmmTime.substring(5,7)));
		time.set(Calendar.SECOND, 0);
		time.set(Calendar.MILLISECOND, 0);
		
		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		
		// basically we're assuming that if the time is more than 1 hour in the past then we need to add 24 hrs so it is in the future
		// (max could be one just short of 1 hour if we are at say 0059 but looking at a full hour which starts at 0000)
		now.add(Calendar.HOUR, -1);
		if( time.before(now) )
			time.add(Calendar.HOUR, 24);

		return time.getTimeInMillis();
	}
	
	/**
	 * Add (or subtract) an amount of minutes to a given date and return the new date.
	 * @param time - the date object you want to add time to
	 * @param timezone - the timezone of the date object
	 * @param amount - the amount of time to add, respective to the unit specified
	 * @param unit - an int representing the units you want to add (use Calendar.MINUTE, Calendar.HOUR, Calendar.SECOND, etc.)
	 * @return a Date object representing the new time
	 */
	public static Date addTime(Date time, String timezone, int amount, int unit) {
		if( time == null || timezone == null )
			return null;
		if( amount==0 )
			return time;
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
		cal.setTime(time);
		cal.add(unit, amount);
		return cal.getTime();
	}
	
	/**
	 * Add (or subtract) an amount of time to a given time and return the new long time. Much more efficient than the Date variant of this method.
	 * @param time - the long value you want to add time to
	 * @param amount - the amount of time to add, respective to the unit specified
	 * @param unit - an int representing the units you want to add (use Calendar.MINUTE, Calendar.HOUR, or Calendar.SECOND, etc.). Default assumes seconds.
	 * @return long time representing the new time
	 */
	public static long addTime(long time, int amount, int unit) {
		if( amount==0 )
			return time;
		switch(unit) {
			case Calendar.HOUR:
				return time+(amount*60*60*1000);
			case Calendar.MINUTE:
				return time+(amount*60*1000);
			default:
				return time+(amount*1000);
		}
	}
	
	/**
	 * Add (or subtract) an amount of time to a given time and return the new long time. Much more efficient than the Date variant of this method.
	 * @param time - the long value you want to add time to
	 * @param amount - the amount of time to add, respective to the unit specified
	 * @param unit - an int representing the units you want to add (use Calendar.MINUTE, Calendar.HOUR, or Calendar.SECOND, etc.). Default assumes seconds.
	 * @return long time representing the new time
	 */
	public static long addTime(long time, long amount, int unit) {
		if( amount==0 )
			return time;
		switch(unit) {
			case Calendar.HOUR:
				return time+(amount*60*60*1000);
			case Calendar.MINUTE:
				return time+(amount*60*1000);
			default:
				return time+(amount*1000);
		}
	}

}
