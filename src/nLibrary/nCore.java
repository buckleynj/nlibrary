package nLibrary;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * A class for general operations, such a printing to the console and handling errors.<br>
 * Set nCore.areLogging to <b>true</b> if you want to log printed console messages by default.
 * @author Nathan Buckley
 *
 */
public class nCore {
	
	final static SimpleDateFormat ft_LOCAL = new SimpleDateFormat ("dd/hh:mm:ss a z");
	final static SimpleDateFormat ft_UTC = new SimpleDateFormat ("dd/HH:mm:ss");
	static {
		ft_UTC.setTimeZone(TimeZone.getTimeZone("UTC"));
	}
	
	/**
	 * Version of this library.
	 */
	final static double version = 1.0;

	/**
	 * By specifying a startTime, the logs can properly show an elapsed time from that time for any future print message.
	 */
	static long startTime = -1;
	
	/**
	 * Sets the start time (long epoch time in ms).
	 * @param logging
	 */
	public static void setStartTime(long time) {
	  startTime = time;
	}
	
	/**
	 * Shorthand for printing a message to the console.
	 * @param msg - The message you want to print to the console.
	 * @param category - (optional) A category you'd like to prepend to the message.
	 */
	public static void say(String msg) {
		say(msg, null);
	}
	
	/**
	 * Shorthand for printing a message to the console.
	 * @param msg - The message you want to print to the console.
	 * @param category - (optional) A category you'd like to prepend to the message.
	 */
	public static void say(String msg, String category) {
		if( msg == null )
			return;
		if( category == null )
			System.out.println(msg);
		else
			System.out.println(category+"\t"+msg);
	}
	
  /**
   * Writes a message to the console with a timestamp in both local and UTC time.
   * The 2nd parameter is the <b>category</b> which is a general category for this type of log event.<br>
   * The 3rd parameter is the <b>unique id</b> which is a unique id which is specific to this and only this message.<br>
   * The 4th parameter is whether the message is <b>logged</b> (i.e. saved to the log file). If omitted, defaults to the state of {@link nLibrary.nCore.areLogging}.<br>
   * @param msg - (String) the message to log
   * @param source - (String) the source of the print message, i.e. the function or part of the code where the print() message resides
   * @param category - (String) the category for the message (optional)
   * @param uniqueid - (String) the unique id for the message (optional)
   * @param logged - (boolean) whether the message is logged (optional)
   */
	final public static void print(String msg) {
		print(msg,"-","UNCATEGORIZED","-",nFile.areLogging);
	}
	
  /**
   * Writes a message to the console with a timestamp in both local and UTC time.
   * The 2nd parameter is the <b>category</b> which is a general category for this type of log event.<br>
   * The 3rd parameter is the <b>unique id</b> which is a unique id which is specific to this and only this message.<br>
   * The 4th parameter is whether the message is <b>logged</b> (i.e. saved to the log file). If omitted, defaults to the state of {@link nLibrary.nCore.areLogging}.<br>
   * @param msg - (String) the message to log
   * @param source - (String) the source of the print message, i.e. the function or part of the code where the print() message resides
   * @param category - (String) the category for the message (optional)
   * @param uniqueid - (String) the unique id for the message (optional)
   * @param logged - (boolean) whether the message is logged (optional)
   */
	final public static void print(String msg, String source) {
		print(msg,source,"UNCATEGORIZED","-",nFile.areLogging);
	}
	
  /**
   * Writes a message to the console with a timestamp in both local and UTC time.
   * The 2nd parameter is the <b>category</b> which is a general category for this type of log event.<br>
   * The 3rd parameter is the <b>unique id</b> which is a unique id which is specific to this and only this message.<br>
   * The 4th parameter is whether the message is <b>logged</b> (i.e. saved to the log file). If omitted, defaults to the state of {@link nLibrary.nCore.areLogging}.<br>
   * @param msg - (String) the message to log
   * @param source - (String) the source of the print message, i.e. the function or part of the code where the print() message resides
   * @param category - (String) the category for the message (optional)
   * @param uniqueid - (String) the unique id for the message (optional)
   * @param logged - (boolean) whether the message is logged (optional)
   */
	final public static void print(String msg, String source, String category) {
		print(msg,source,category,"-",nFile.areLogging);
	}
	
  /**
   * Writes a message to the console with a timestamp in both local and UTC time.
   * The 2nd parameter is the <b>category</b> which is a general category for this type of log event.<br>
   * The 3rd parameter is the <b>unique id</b> which is a unique id which is specific to this and only this message.<br>
   * The 4th parameter is whether the message is <b>logged</b> (i.e. saved to the log file). If omitted, defaults to the state of {@link nLibrary.nCore.areLogging}.<br>
   * @param msg - (String) the message to log
   * @param source - (String) the source of the print message, i.e. the function or part of the code where the print() message resides
   * @param category - (String) the category for the message (optional)
   * @param uniqueid - (String) the unique id for the message (optional)
   * @param logged - (boolean) whether the message is logged (optional)
   */
  final public static void print(String msg, String source, String category, String uniqueid) {
    print(msg,source,category,uniqueid,nFile.areLogging);
  }
	
	/**
	 * Writes a message to the console with a timestamp in both local and UTC time.
	 * The 2nd parameter is the <b>category</b> which is a general category for this type of log event.<br>
	 * The 3rd parameter is the <b>unique id</b> which is a unique id which is specific to this and only this message.<br>
	 * The 4th parameter is whether the message is <b>logged</b> (i.e. saved to the log file). If omitted, defaults to the state of {@link nLibrary.nCore.areLogging}.<br>
	 * @param msg - (String) the message to log
	 * @param source - (String) the source of the print message, i.e. the function or part of the code where the print() message resides
	 * @param category - (String) the category for the message (optional)
	 * @param uniqueid - (String) the unique id for the message (optional)
	 * @param logged - (boolean) whether the message is logged (optional)
	 */
	final public static void print(String msg, String source, String category, String uniqueid, boolean logged) {
		// if there's no message, return, otherwise trim it down
		if( msg == null || msg.trim().equals("") )
			return;
		else
			msg = msg.trim();
		
		final Date d = new Date();
		
		final String tLine = 
		    String.valueOf(d.getTime()) + "\t"    // epoch time
		    + ft_UTC.format(d) + "\t"             // UTC time 
		    + ft_LOCAL.format(d) + "\t"           // Local time
		    + (startTime==-1?"-":nTime.getElapsedTime(d.getTime())) + "\t"  // elapsed time from the start time (if one was set)
		    + category + "\t"                     // category
		    + uniqueid + "\t"                     // unique identifier
		    + source + "\t"                       // source
		    + msg;                                // the message itself

		System.out.println(tLine);
		
		// if we're logging, log it (we log everything and sort it out later)
		if( logged ) {
			nFile.logline(tLine);
		}
	}
	
	public static void handleStackTrace(Throwable e) {
		StackTraceElement[] ste = e.getStackTrace();
		String info = "Caught exception:\n===================================================\n"; 
		info += e.toString() + "\n";
		for(StackTraceElement element : ste) {
			info+=element.toString()+"\n";
		}
		info += "===================================================\n";
		print(info,"ERROR");
		//e.printStackTrace();
	}
	
	/**
	 * Given a primitive type name and associated data, returns an instance of that class with the provided data.<br>
	 * Class names follow the convention used by class.getClass().getSimpleName(), e.g. 'Integer', 'Double', 'String[]'<br>
	 * Assumes any array data is delimited by en dashes ("\u2013").<br>
	 * Handles all Java primitive types except Char.
	 * @param className
	 * @param data
	 * @return
	 */
    public static Object makeObject(String className, String data) {
    	switch(className.toLowerCase()) {
	    	case "string":
	    		return new String(data);
	    	case "string[]":
	    		return data.split("\u2013");
	    	case "boolean":
	    		return Boolean.parseBoolean(data);
	    	case "boolean[]":
	    		String[] boolArray = data.split("\u2013");
	    		Boolean[] booly = new Boolean[boolArray.length];
	    		for(int i=0;i<boolArray.length;i++) {
	    			booly[i] = Boolean.parseBoolean(boolArray[i]);
	    		}
	    		return booly;
	    	case "int":
	    	case "integer":
	    		return Integer.parseInt(data);
	    	case "integer[]":
	    		String[] intArray = data.split("\u2013");
	    		Integer[] inty = new Integer[intArray.length];
	    		for(int i=0;i<intArray.length;i++) {
	    			inty[i] = Integer.parseInt(intArray[i]);
	    		}
	    		return inty;
	    	case "double":
	    		return Double.parseDouble(data);
	    	case "double[]":
	    		String[] dblArray = data.split("\u2013");
	    		Double[] dbly = new Double[dblArray.length];
	    		for(int i=0;i<dblArray.length;i++) {
	    			dbly[i] = Double.parseDouble(dblArray[i]);
	    		}
	    		return dbly;
	    	case "float":
	    		return Float.parseFloat(data);
	    	case "float[]":
	    		String[] fltArray = data.split("\u2013");
	    		Float[] flty = new Float[fltArray.length];
	    		for(int i=0;i<fltArray.length;i++) {
	    			flty[i] = Float.parseFloat(fltArray[i]);
	    		}
	    		return flty;
	    	case "long":
	    		return Long.parseLong(data);
	    	case "long[]":
	    		String[] lngArray = data.split("\u2013");
	    		Long[] lngy = new Long[lngArray.length];
	    		for(int i=0;i<lngArray.length;i++) {
	    			lngy[i] = Long.parseLong(lngArray[i]);
	    		}
	    		return lngy;
	    	case "byte":
	    		return Byte.parseByte(data);
	    	case "byte[]":
	    		String[] byteArray = data.split("\u2013");
	    		Byte[] bytey = new Byte[byteArray.length];
	    		for(int i=0;i<byteArray.length;i++) {
	    			bytey[i] = Byte.parseByte(byteArray[i]);
	    		}
	    		return bytey;
	    	case "short":
	    		return Short.parseShort(data);
	    	case "short[]":
	    		String[] shortArray = data.split("\u2013");
	    		Short[] shorty = new Short[shortArray.length];
	    		for(int i=0;i<shortArray.length;i++) {
	    			shorty[i] = Short.parseShort(shortArray[i]);
	    		}
	    		return shorty;
    	}
    	return null;
    }
	
    /**
     * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.<br>
     * Opposite of String.split(). If not specified, the delimiter is a comma.
     * @param array
     * @param delimiter
     * @return
     */
	public static String join(String[] array) {
		return join(array,null);
	}
	
	/**
     * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.<br>
     * Opposite of String.split(). If not specified, the delimiter is a comma.
     * @param array
     * @param delimiter
     * @return
     */
    public static String join(ArrayList<String> array) {
        return join(array,null);
    }
	
    /**
     * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.<br>
     * Opposite of String.split(). If not specified, the delimiter is a comma.
     * @param array
     * @param delimiter
     * @return
     */
	public static String join(String[] array, String delimiter) {
		if( delimiter == null )
			delimiter = ",";
		if( array == null || array.length == 0 )
			return "";
		StringBuilder sb = new StringBuilder();
		for(String str : array) {
			sb.append(str).append(delimiter);
		}
		// remove trailing delimiter
		sb.setLength(sb.length()-delimiter.length());
		return sb.toString();
	}
	
    /**
     * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.<br>
     * Opposite of String.split(). If not specified, the delimiter is a comma.
     * @param array
     * @param delimiter
     * @return
     */
    public static String join(ArrayList<String> array, String delimiter) {
        if( delimiter == null )
            delimiter = ",";
        if( array == null || array.isEmpty() )
            return "";
        StringBuilder sb = new StringBuilder();
        for(String str : array) {
            sb.append(str).append(delimiter);
        }
        // remove trailing delimiter
        sb.setLength(sb.length()-delimiter.length());
        return sb.toString();
    }
	
	/**
	 * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.
	 * @param array
	 * @param delimiter
	 * @return
	 */
	public static String toString(String[] array) {
		return toString(array,", ");
	}
	
	/**
     * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.
     * @param array
     * @param delimiter
     * @return
     */
    public static String toString(ArrayList<String> array) {
        return join(array,", ");
    }
	
	/**
	 * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.
	 * @param array
	 * @param delimiter
	 * @return
	 */
	public static String toString(String[] array, String delimiter) {
		return join(array, delimiter);
	}
	
	/**
     * Given an array or ArrayList, return a single String with the entire contents of the array with the delimiter of your choice.
     * @param array
     * @param delimiter
     * @return
     */
    public static String toString(ArrayList<String> array, String delimiter) {
        return join(array, delimiter);
    }

}
