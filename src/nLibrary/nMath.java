package nLibrary;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * A class for doing basic math operations.
 * @author Nathan Buckley
 *
 */
public class nMath {
	
    /**
     * Given a long elapsed time in milliseconds, returns a String corresponding to that time in an
     * appropriate unit. "Appropriate" meaning:
     * <ul>
     * <li>If the time is less than 1000 ms, an ms value is used</li>
     * <li>If the time is less than 60 seconds, a seconds value is used</li>
     * <li>If the time is less than 60 minutes, a minutes value is used</li>
     * <li>If the time is greater than or equal to 1 hour, an hours value is used</li>
     * </ul>
     * See also: {@link nLibrary.nTime#getHumanReadableDurationFromMilliseconds
     * nTime.getHumanReadableDurationFromMilliseconds()}
     * 
     * @return
     */
    public static String displayLongElapsedTimeAsAppropriateValue(long ms) {
        final long milliseconds = Math.abs(ms);
        final double seconds = milliseconds / 1000D;
        final double minutes = seconds / 60D;
        final double hours = minutes / 60D;
        return (milliseconds < 1000 ? milliseconds + " milliseconds" : (seconds < 60 ? round(seconds, 1) + " seconds" : (minutes < 60 ? round(minutes, 1) + " minutes" : round(hours, 1) + " hours")));
    }
	
    /**
     * Returns true if the two provided longs are within the specified ms value two each other.<br>
     * <br>
     * Useful for determining when two numbers, such as epoch times, are close enough to each other so
     * as to be effectively the same in a given context. For example, departureTimes in MACS are often
     * stored as relative times, and because of rounding in various places and the fact that their
     * related epoch times often need to be dynamically generated based on elapsed time, sometimes there
     * will be slight deviations from one calculation to the next. When determining whether a departure
     * time has changed -- truly changed, and not just on the order of a few milliseconds due to the
     * aforementioned variance -- we can use this method to make sure that the two compared times are
     * truly different outside the normal the tolerance levels we can expect.
     *
     * @param long1    - the first long (order does not matter)
     * @param long2    - the second long (order does not matter)
     * @param howClose - how close they should be in milliseconds (if the difference between them is this amount or less
     *                 then this function will return true, otherwise false)
     */
    public static boolean areClose(long long1, long long2, long howClose) {
        // we can return true right away if the longs are the same
        if ( long1 == long2 )
            return true;

        // now to see if the two longs are within the tolerance provided, just subtract
        // the two values and compare the absolute value of the difference with the desired value
        final long difference = Math.abs(long1 - long2);
        if ( difference <= howClose )
            return true;
        return false;
    }
	
	/**
	 * Returns true if the supplied int value is even, otherwise false.
	 * @param num
	 * @return
	 */
	public static boolean isEven(int num) {
		if( (num & 1) == 0 )
			return true;
		return false;
	}
	
	/**
	 * Returns true if the supplied int value is odd, otherwise false.
	 * @param num
	 * @return
	 */
	public static boolean isOdd(int num) {
		if( isEven(num) )
			return false;
		return true;
	}
	
	/**
	 * Returns a random number within a specified range.
	 * @param min
	 * @param max
	 * @return
	 */
	public static int random(int min, int max) {
		//Random rand = new Int();
		//return rand.nextInt((max - min) + 1) + min;
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	/**
	 * Returns the fractional portion of a given double.
	 * @param num (double)
	 * @return
	 */
	public static double getFractionalPortion(double num) {
		return num % 1;
	}
	
	/**
	 * Returns the fractional portion of a given float.
	 * @param num (float)
	 * @return
	 */
	public static float getFractionalPortion(float num) {
		return num % 1;
	}
	/**
	 * Returns a double rounded to a specified number of decimal places.
	 * @param number
	 * @param decimalplaces
	 * @return
	 */
	public static double round(double number, int decimalplaces) {
		if( decimalplaces < 1 )
			return Math.round(number);
		return Math.round(number*Math.pow(10,(double)decimalplaces))/Math.pow(10,(double)decimalplaces);
	}
	
	/**
	 * Returns a double percent value, based on a given fraction between the first two parameters, rounded to a specified number of decimal places.
	 * @param numa
	 * @param numb
	 * @param decimalplaces
	 * @return
	 */
	public static double percent(double numa, double numb, int decimalplaces) {
		return numa==0?0:round((numa/numb)*100D,decimalplaces);
	}
	
	// milliseconds to seconds
	/**
	 * Converts an input milliseconds value to seconds, rounded to a specified number of decimal places.
	 * @param ms - a long value in milliseconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in seconds
	 */
	public static double ms2sec(long ms) {
		return ms==0?0:ms2sec(ms,1);
	}
	
	/**
	 * Converts an input milliseconds value to seconds, rounded to a specified number of decimal places.
	 * @param ms - a long value in milliseconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in seconds
	 */
	public static double ms2sec(long ms, int decimalplaces) {
		return ms==0?0:round(ms/1000D,decimalplaces);
	}
	
	// milliseconds to minutes
	/**
	 * Converts an input milliseconds value to minutes, rounded to a specified number of decimal places.
	 * @param ms - a long value in milliseconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in minutes
	 */
	public static double ms2min(long ms) {
		return ms==0?0:ms2min(ms,1);
	}
	
	/**
	 * Converts an input milliseconds value to minutes, rounded to a specified number of decimal places.
	 * @param ms - a long value in milliseconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in minutes
	 */
	public static double ms2min(long ms, int decimalplaces) {
		return ms==0?0:round(ms/1000D/60D,decimalplaces);
	}
	
	// milliseconds to hours
	/**
	 * Converts an input milliseconds value to hours, rounded to a specified number of decimal places.
	 * @param ms - a long value in milliseconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in hours
	 */
	public static double ms2hr(long ms) {
		return ms==0?0:ms2hr(ms,1);
	}
	
	/**
	 * Converts an input milliseconds value to hours, rounded to a specified number of decimal places.
	 * @param ms - a long value in milliseconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in hours
	 */
	public static double ms2hr(long ms, int decimalplaces) {
		return ms==0?0:round(ms/1000D/60D/60D,decimalplaces);
	}
	
	// seconds to minutes
	/**
	 * Converts an input seconds value to minutes, rounded to a specified number of decimal places.
	 * @param sec - a long value in seconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in minutes
	 */
	public static double sec2min(long sec) {
		return sec==0?0:sec2min(sec,1);
	}
	
	/**
	 * Converts an input seconds value to minutes, rounded to a specified number of decimal places.
	 * @param sec - a long value in seconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in minutes
	 */
	public static double sec2min(long sec, int decimalplaces) {
		return sec==0?0:round(sec/60D,decimalplaces);
	}
	
	// seconds to hours
	/**
	 * Converts an input seconds value to hours, rounded to a specified number of decimal places.
	 * @param sec - a long value in seconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in hours
	 */
	public static double sec2hr(long sec) {
		return sec==0?0:sec2hr(sec,1);
	}
	
	/**
	 * Converts an input seconds value to hours, rounded to a specified number of decimal places.
	 * @param sec - a long value in seconds
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in hours
	 */
	public static double sec2hr(long sec, int decimalplaces) {
		return sec==0?0:round(sec/60D/60D,decimalplaces);
	}
	
	// minutes to hours
	/**
	 * Converts an input minutes value to hours, rounded to a specified number of decimal places.
	 * @param min - a long value in minutes
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in hours
	 */
	public static double min2hr(long min) {
		return min==0?0:min2hr(min,1); // could also just call sec2min since it's the same transformation but let's not confuse people....
	}
	
	/**
	 * Converts an input minutes value to hours, rounded to a specified number of decimal places.
	 * @param min - a long value in minutes
	 * @param decimalplaces - (optional) If omitted, defaults to 1 decimal place.
	 * @return a double value in hours
	 */
	public static double min2hr(long min, int decimalplaces) {
		return min==0?0:round(min/60D,decimalplaces);
	}
	
	/**
	 * Converts an input minutes value to hours, rounded to a specified number of decimal places.
	 * @param min - a long value in minutes
	 * @return a double value in hours
	 */
	public static double min2ms(long min) {
		return min==0?0:min*60D*1000D;
	}
	
	/**
	 * Returns an Integer representing the lowest value in the list.
	 * @param list
	 * @return the lowest value or null if the list is empty
	 */
	public static Integer getLowestValue(List<Integer> list) {
		if(list.isEmpty())
			return null;
		Collections.sort(list);
		return list.get(0);
	}
	
	/**
	 * Returns an Integer representing the highest value in the list.
	 * @param list
	 * @return the highest value or null if the list is empty
	 */
	public static Integer getHighestValue(List<Integer> list) {
		if(list.isEmpty())
			return null;
		Collections.sort(list);
		return list.get(list.size()-1);
	}
	
	// Should use generics but for now this is simpler...
	/**
	 * Returns an Integer <b>range</b> from an Integer list.<br>
	 * @param list
	 * @return String in the format "range,lowestvalue,highestvalue"
	 */
	public static Integer getRange(List<Integer> list) {
		if(list.isEmpty())
			return null;
		Integer highestvalue = null,lowestvalue = null;
		for( Integer i : list ) {
			if( highestvalue == null )
				highestvalue = i;
			else if( i > highestvalue )
				highestvalue = i;
			if( lowestvalue == null )
				lowestvalue = i;
			else if( i < lowestvalue )
				lowestvalue = i;
		}
		return Math.abs(highestvalue-lowestvalue);
	}
	
	/**
	 * Returns a String <b>range</b> with the lowest and highest values from an Integer list.<br>
	 * @param list
	 * @return String in the format "range (lowestvalue-highestvalue)"
	 */
	public static String getRangeInfo(List<Integer> list) {
		if(list.isEmpty())
			return null;
		Integer highestvalue = null,lowestvalue = null;
		for( Integer i : list ) {
			if( highestvalue == null )
				highestvalue = i;
			else if( i > highestvalue )
				highestvalue = i;
			if( lowestvalue == null )
				lowestvalue = i;
			else if( i < lowestvalue )
				lowestvalue = i;
		}
		return String.valueOf(Math.abs(highestvalue-lowestvalue)) + " (" + String.valueOf(lowestvalue) + "-" + String.valueOf(highestvalue)+")";
	}
	
	/**
	 * Returns an long <b>range</b> from a Long list.<br>
	 * @param list
	 * @return String in the format "range,lowestvalue,highestvalue"
	 */
	public static Long getLongRange(List<Long> list) {
		if(list.isEmpty())
			return null;
		Long highestvalue = null,lowestvalue = null;
		for( Long i : list ) {
			if( highestvalue == null )
				highestvalue = i;
			else if( i > highestvalue )
				highestvalue = i;
			if( lowestvalue == null )
				lowestvalue = i;
			else if( i < lowestvalue )
				lowestvalue = i;
		}
		return Math.abs(highestvalue-lowestvalue);
	}
	
	/**
	 * Returns an String <b>range</b> with the lowest and highest values from a Long list.<br>
	 * @param list
	 * @return String in the format "range (lowestvalue-highestvalue)"
	 */
	public static String getLongRangeInfo(List<Long> list) {
		if(list.isEmpty())
			return null;
		Long highestvalue = null,lowestvalue = null;
		for( Long i : list ) {
			if( highestvalue == null )
				highestvalue = i;
			else if( i > highestvalue )
				highestvalue = i;
			if( lowestvalue == null )
				lowestvalue = i;
			else if( i < lowestvalue )
				lowestvalue = i;
		}
		return String.valueOf(Math.abs(highestvalue-lowestvalue)) + " (" + String.valueOf(lowestvalue) + "-" + String.valueOf(highestvalue)+")";
	}
	
	
	/**
	 * Returns an long <b>range</b> from a Double list.<br>
	 * @param list
	 * @return String in the format "range,lowestvalue,highestvalue"
	 */
	public static Double getDoubleRange(List<Double> list) {
		if(list.isEmpty())
			return null;
		Double highestvalue = null,lowestvalue = null;
		for( Double i : list ) {
			if( highestvalue == null )
				highestvalue = i;
			else if( i > highestvalue )
				highestvalue = i;
			if( lowestvalue == null )
				lowestvalue = i;
			else if( i < lowestvalue )
				lowestvalue = i;
		}
		return Math.abs(highestvalue-lowestvalue);
	}
	
	/**
	 * Returns an String <b>range</b> with the lowest and highest values from a Double list.<br>
	 * @param list
	 * @return String in the format "range (lowestvalue-highestvalue)"
	 */
	public static String getDoubleRangeInfo(List<Double> list) {
		if(list == null || list.isEmpty())
			return null;
		Double highestvalue = null,lowestvalue = null;
		for( Double i : list ) {
			if( highestvalue == null )
				highestvalue = i;
			else if( i > highestvalue )
				highestvalue = i;
			if( lowestvalue == null )
				lowestvalue = i;
			else if( i < lowestvalue )
				lowestvalue = i;
		}
		if( highestvalue == null && lowestvalue == null )
			return "All "+list.size()+" values were null.";
		else if( highestvalue == null )
			return "0 ("+String.valueOf(highestvalue)+"-"+String.valueOf(highestvalue)+")";
		else if( lowestvalue == null )
			return "0 ("+String.valueOf(lowestvalue)+"-"+String.valueOf(lowestvalue)+")";
		else
			return String.valueOf(Math.abs(highestvalue-lowestvalue)) + " (" + String.valueOf(lowestvalue) + "-" + String.valueOf(highestvalue)+")";
	}
	
	/**
	 * Returns a Double <b>median</b> value from an Integer list.<br>
	 * The <b>median</b> is the middle value in a sorted (ordinal) set of numbers. If the list contains an even number, the 2 middle numbers are averaged.
	 * @param list
	 * @return
	 */
	public static Double getMedian(List<Integer> list) {
		try {
			if(list==null || list.isEmpty())
				return null;
			if( list.size() == 1 )
				return (double)list.get(0);
			// need to remove null values or it will cause a NPE error
			List<Integer> indicesToRemove = new ArrayList<>();
			for(int i=0;i<list.size();i++) {
				if(list.get(i)==null)
					indicesToRemove.add(i);
			}
			// now remove these indices in descending order (if we remove smaller indices first it will offset the later ones)
			for(int i=indicesToRemove.size()-1;i>=0;i--)
				list.remove(indicesToRemove.get(i).intValue());
			
			// return null if there are no remaining values
			if(list==null || list.isEmpty())
				return null;
			
			// sort the list
			Collections.sort(list);
			
			double median;
			if (list.size() % 2 == 0)
			    median = ((double)list.get(list.size()/2) + (double)list.get(list.size()/2-1))/2d;
			else
			    median = (double) list.get(list.size()/2);
			return median;
		} catch(Exception e) {
			nCore.handleStackTrace(e);
			return null;
		}
	}
	
	/**
	 * Returns a Double <b>median</b> value from an Long list.<br>
	 * The <b>median</b> is the middle value in a sorted (ordinal) set of numbers. If the list contains an even number, the 2 middle numbers are averaged.
	 * @param list
	 * @return
	 */
	public static Double getLongMedian(List<Long> list) {
		try {
			if(list==null || list.isEmpty())
				return null;
			if( list.size() == 1 )
				return (double)list.get(0);
			// need to remove null values or it will cause a NPE error
			List<Integer> indicesToRemove = new ArrayList<>();
			for(int i=0;i<list.size();i++) {
				if(list.get(i)==null)
					indicesToRemove.add(i);
			}
			// now remove these indices in descending order (if we remove smaller indices first it will offset the later ones)
			for(int i=indicesToRemove.size()-1;i>=0;i--)
				list.remove(indicesToRemove.get(i).intValue());  // have to cast it to int because otherwise it thinks it's an object
			
			// return null if there are no remaining values
			if(list==null || list.isEmpty())
				return null;
			
			// sort the list
			Collections.sort(list);
			
			double median;
			if (list.size() % 2 == 0)
			    median = ((double)list.get(list.size()/2) + (double)list.get(list.size()/2-1))/2d;
			else
			    median = (double) list.get(list.size()/2);
			return median;
		} catch(Exception e) {
			nCore.handleStackTrace(e);
			return null;
		}
	}
	
	
	/**
	 * Returns a Double <b>median</b> value from an Double list.<br>
	 * The <b>median</b> is the middle value in a sorted (ordinal) set of numbers. If the list contains an even number, the 2 middle numbers are averaged.
	 * @param list
	 * @return
	 */
	public static Double getDoubleMedian(List<Double> list) {
		try {
			if(list==null || list.isEmpty())
				return null;
			if( list.size() == 1 && list.get(0) != null )
				return (double)list.get(0);
			// need to remove null values or it will cause a NPE error
			List<Integer> indicesToRemove = new ArrayList<>();
			for(int i=0;i<list.size();i++) {
				if(list.get(i)==null)
					indicesToRemove.add(i);
			}
			// now remove these indices in descending order (if we remove smaller indices first it will offset the later ones)
			for(int i=indicesToRemove.size()-1;i>=0;i--)
				list.remove(indicesToRemove.get(i).intValue());  // have to cast it to int because otherwise it thinks it's an object
			
			// return null if there are no remaining values
			if(list==null || list.isEmpty())
				return null;
			
			// sort the list
			Collections.sort(list);
			
			double median;
			if (list.size() % 2 == 0)
			    median = (list.get(list.size()/2) + list.get(list.size()/2-1))/2d;
			else
			    median = list.get(list.size()/2);
			return median;
		} catch(Exception e) {
			nCore.handleStackTrace(e);
			return null;
		}
	}

	/**
	 * Return the <b>mode</b> of all the values in an Integer list.<br>
	 * The <b>mode</b> is the value that occurs the most in a set.<br>
	 * TODO: This needs to be updated because there can be more than one most common value if there's a tie. Basically can only use getModeInfo.
	 * @param list
	 * @return - An Integer mode of the list's values
	 */
	public static Integer getMode(List<Integer> list) {
		if(list.isEmpty())
			return null;
		TreeMap<Integer, Integer> values = new TreeMap<>();  // the integer | the count
		// count the # of each value
		for (Integer i : list) {
			if( values.get(i) != null ) {
				values.put(i,values.get(i)+1);
			}
			else {
				values.put(i,1);
			}
		}
		
		// find the biggest one
		int highestCount = 0;
		int mostCommonValue = 0;
		for(Entry<Integer, Integer> entry : values.entrySet()) {
			if( entry.getValue() > highestCount ) {
				highestCount = entry.getValue();
				mostCommonValue = entry.getKey();
			}
		}
	    return mostCommonValue;
	}
	
	/**
	 * Return the <b>mode</b> (value which appears the most) of all the values in an Integer list and how many times it was counted.
	 * The <b>mode</b> is the value that occurs the most in a set.
	 * @param list
	 * @return - A String mode of the list's values in the format "mode (count)"
	 */
	public static String getModeInfo(List<Integer> list) {
		if(list.isEmpty())
			return null;
		Integer mode = 0;
		HashMap<Integer, Integer> values = new HashMap<>();  // the integer | it's count
		// count the # of each value
		for (Integer i : list) {
			if( values.get(i) != null ) {
				values.put(i,values.get(i)+1);
			}
			else {
				values.put(i,1);
			}
		}
		
		// find the one with the highest count in our list of values we recorded
		int highestCount = 0;
		Iterator<Entry<Integer, Integer>> it = values.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pairs = it.next();
			if( pairs.getValue() > highestCount ) {
				highestCount = pairs.getValue();
				mode = pairs.getKey();
			}
			it.remove();
		}
		return String.valueOf(mode)+" ("+highestCount+")";
	}

	/**
	 * Return the <b>mode</b> of all the values in an Long list.<br>
	 * The <b>mode</b> is the value that occurs the most in a set.
	 * TODO: This needs to be updated because there can be more than one most common value if there's a tie. Basically can only use getModeInfo.
	 * @param list
	 * @return - An Long mode of the list's values
	 */
	public static Long getLongMode(List<Long> list) {
		if(list.isEmpty())
			return null;
		TreeMap<Long, Integer> values = new TreeMap<>();  // the Long | the count
		// count the # of each value
		for (Long i : list) {
			if( values.get(i) != null ) {
				values.put(i,values.get(i)+1);
			}
			else {
				values.put(i,1);
			}
		}
		
		// find the biggest one
		int highestCount = 0;
		long mostCommonValue = 0;
		for(Entry<Long, Integer> entry : values.entrySet()) {
			if( entry.getValue() > highestCount ) {
				highestCount = entry.getValue();
				mostCommonValue = entry.getKey();
			}
		}
	    return mostCommonValue;
	}
	
	/**
	 * Return the <b>mode</b> (value which appears the most) of all the values in an Long list and how many times it was counted.
	 * The <b>mode</b> is the value that occurs the most in a set.
	 * @param list
	 * @return - A String mode of the list's values in the format "mode (count)"
	 */
	public static String getLongModeInfo(List<Long> list) {
		if(list.isEmpty())
			return null;
		Long mode = 0L;
		HashMap<Long, Integer> values = new HashMap<>();  // the Long | it's count
		// count the # of each value
		for (Long i : list) {
			if( values.get(i) != null ) {
				values.put(i,values.get(i)+1);
			}
			else {
				values.put(i,1);
			}
		}
		
		// find the one with the highest count in our list of values we recorded
		int highestCount = 0;
		Iterator<Entry<Long, Integer>> it = values.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Long, Integer> pairs = it.next();
			if( pairs.getValue() > highestCount ) {
				highestCount = pairs.getValue();
				mode = pairs.getKey();
			}
			it.remove();
		}
		return String.valueOf(mode)+" ("+highestCount+")";
	}
	
	/**
	 * Return the <b>mode</b> of all the values in an Long list.<br>
	 * The <b>mode</b> is the value that occurs the most in a set.
	 * TODO: This needs to be updated because there can be more than one most common value if there's a tie. Basically can only use getModeInfo.
	 * @param list
	 * @return - An Long mode of the list's values
	 */
	public static Double getDoubleMode(List<Double> list) {
		if(list.isEmpty())
			return null;
		TreeMap<Double, Integer> values = new TreeMap<>();  // the Double | the count
		// count the # of each value
		for (Double i : list) {
			if( values.get(i) != null ) {
				values.put(i,values.get(i)+1);
			}
			else {
				values.put(i,1);
			}
		}
		
		// find the biggest one
		int highestCount = 0;
		double mostCommonValue = 0;
		for(Entry<Double, Integer> entry : values.entrySet()) {
			if( entry.getValue() > highestCount ) {
				highestCount = entry.getValue();
				mostCommonValue = entry.getKey();
			}
		}
	    return mostCommonValue;
	}
	
	/**
	 * Return the <b>mode</b> (value which appears the most) of all the values in an Long list and how many times it was counted.
	 * The <b>mode</b> is the value that occurs the most in a set.
	 * @param list
	 * @return - A String mode of the list's values in the format "mode (count)"
	 */
	public static String getDoubleModeInfo(List<Double> list) {
		if(list.isEmpty())
			return null;
		Double mode = 0D;
		HashMap<Double, Integer> values = new HashMap<>();  // the Double | it's count
		// count the # of each value
		for (Double i : list) {
			if( values.get(i) != null ) {
				values.put(i,values.get(i)+1);
			}
			else {
				values.put(i,1);
			}
		}
		
		// find the one with the highest count in our list of values we recorded
		int highestCount = 0;
		Iterator<Entry<Double, Integer>> it = values.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Double, Integer> pairs = it.next();
			if( pairs.getValue() > highestCount ) {
				highestCount = pairs.getValue();
				mode = pairs.getKey();
			}
			it.remove();
		}
		return String.valueOf(mode)+" ("+highestCount+")";
	}
	
	/**
	 * Return a double <b>mean<b> of all the values in a list.<br>
	 * The <b>mean<b> is the mathematical average of a set of numbers.
	 * @param list
	 * @return - A double mean of the list's values
	 */
	public static Double getMean(List<Integer> list) {
		if(list.isEmpty())
			return null;
		Integer sum = 0;
		for (Integer i : list) {
			if( i != null )
				sum += i;
		}
		return sum.doubleValue() / list.size();
	}
	
	/**
	 * Return a Double <b>mean<b> of all the values in a list.<br>
	 * The <b>mean<b> is the mathematical average of a set of numbers.
	 * @param list
	 * @return - A Long mean of the list's values
	 */
	public static Double getLongMean(List<Long> list) {
		if(list.isEmpty())
			return null;
		Long sum = 0L;
		for (Long i : list) {
			if( i != null )
				sum += i;
		}
		return sum.doubleValue() / list.size();
	}
	
	/**
	 * Return a Double <b>mean<b> of all the values in a list.<br>
	 * The <b>mean<b> is the mathematical average of a set of numbers.
	 * @param list
	 * @return - A Long mean of the list's values
	 */
	public static Double getDoubleMean(List<Double> list) {
		if(list.isEmpty())
			return null;
		Double sum = 0D;
		for (Double i : list) {
			if( i != null )
				sum += i;
		}
		return sum / list.size();
	}
	
	/**
	 * Return an Integer sum of all the values in a list.<br>
	 * @param list
	 * @return - A sum of the list's values
	 */
	public static Integer getIntegerSum(List<Integer> list) {
		if(list.isEmpty())
			return null;
		Integer sum = 0;
		for (Integer i : list) {
			if( i != null )
				sum += i;
		}
		return sum;
	}
	
	/**
	 * Return a Double sum of all the values in a list.<br>
	 * @param list
	 * @return - A sum of the list's values
	 */
	public static Double getDoubleSum(List<Double> list) {
		if(list.isEmpty())
			return null;
		Double sum = 0D;
		for (Double i : list) {
			if( i != null )
				sum += i;
		}
		return sum;
	}
	
	/**
	 * Return a Long sum of all the values in a list.<br>
	 * @param list
	 * @return - A sum of the list's values
	 */
	public static Long getLongSum(List<Long> list) {
		if(list.isEmpty())
			return null;
		Long sum = 0L;
		for (Long i : list) {
			if( i != null )
				sum += i;
		}
		return sum;
	}
}
