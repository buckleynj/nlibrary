package nLibrary;

import java.awt.EventQueue;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;


/**
 * A class for handling application threading and background processing.
 * @author Nathan Buckley
 *
 */
public class nThread {

	/**
	 * Invokes the EDT if we're not already on it. Takes a runnable.<br>
	 * For Thread safety, one should always create/change/update Swing JComponents on the EDT.<br><br>
	 * <b>Java 8:</b> <code>Runnable task = () -> {};</code><br>
	 * <b>Java 7:</b> <code>new Runnable() {public void run() {}};</code><br>
	 * <br>
	 * Can alternatively start it in a separate thread (not EDT) manually:<br>
	 * <code>Thread t = new Thread(task);<br>
     * t.start();</code>
	 * @param runnable
	 */
	public static void EDT(Runnable runnable) {
	    if (EventQueue.isDispatchThread()) {
	        runnable.run();
	    } else {
	        SwingUtilities.invokeLater(runnable);
	    }
	}
	
	/**
	 * Create a class for having a clock?
	 * classes for opening new threads?
	 * @author njbuckle
	 *
	 */
    public static class BackgroundProcess extends SwingWorker<Void, Void> {
    	int updateRate = 1000;
		int i = 1;
		Object obj;
		
    	public BackgroundProcess(Object obj) {
    		//frameObj=obj;
    	}
    	
    	@Override
    	protected Void doInBackground() {
    		while (!isCancelled()) {
    			
    			// do something
    			
    			
				try { Thread.sleep(updateRate); } catch (InterruptedException e) {
					nCore.handleStackTrace(e);
				}
				if (i > 99) i = 0; else i++;
    		}
    		return null;
    	}
    	
    	@Override
    	public void done() {
    		nCore.print("BackgroundProcess interuppted.","WARNING");
    	}
    }
	
}
